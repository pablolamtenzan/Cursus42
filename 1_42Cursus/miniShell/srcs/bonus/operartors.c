/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operartors.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/27 18:49:11 by plamtenz          #+#    #+#             */
/*   Updated: 2020/01/27 21:06:08 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* pseudocode for start */

int     manage_contionals(int *ret_bkp, char operator, int *subsh_nb)
{
    if (operator == '(')
    {
        new_subshell(); //fork ?
        *subsh_nb++;
    }
    if (*subsh_nb)
    {
        if (operator == ')')
            *subsh_nb--;
        if ((*ret_bkp && operator == "||") || (!*ret_bkp && operator == "&&"))
            *ret_bkp += minishell(); //command is scope
        if ((!*ret_bkp && operator == "||") || (*ret_bkp && operator == "&&"))
            ret_bkp = 1; //means stop execution in () but continue after it
    }
    else
    {
        if ((*ret_bkp && operator == "||") || (!*ret_bkp && operator == "&&"))
            return (0); // means continue
        if ((!*ret_bkp && operator == "||") || (*ret_bkp && operator == "&&"))
            return (1); // means stop execution 
    }
    *ret_bkp = *ret_bkp > 0 ? 1 : 0;
    
    
        
}

