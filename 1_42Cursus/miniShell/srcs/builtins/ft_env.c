/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_env.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <chamada@student.le-101.fr>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2020/01/14 09:44:45 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2020/01/15 12:52:29 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <map.h>
#include <libft.h>

int	ft_env(t_map *env)
{
	if (env)
	{
		ft_env(env->next);
		ft_printf("%s=%s\n", env->key, env->value);
	}
	return (0);
}
