/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   pf_putchar.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/17 20:17:29 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/01 23:20:14 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_printf.h"

void		pf_putchar(t_printf *p, unsigned c)
{
	p->printed = 1;
	if ((p->padding = p->min_lenght - p->printed) < 0)
		p->padding = 0;
	pf_padding(p, FALSE);
	write(p->fd, &c, 1);
	p->len += p->printed;
	pf_padding(p, TRUE);
}

void		pf_secure_putstr(t_printf *p)
{
	int		min;
	char	*str;

	min = p->precision >= 0 ? p->precision : 6;
	if (p->precision == 0 && p->min_lenght == 0)
		str = (char *)va_arg(p->ap, unsigned*);
	else if (p->precision == 0 && p->min_lenght != 0)
	{
		p->padding = p->min_lenght;
		pf_padding(p, FALSE);
		pf_padding(p, TRUE);
		str = (char *)va_arg(p->ap, unsigned*);
	}
	else
		pf_putstr(p, min);
}

static void	pf_null_str(t_printf *p, int min)
{
	if (!(p->f & F_ZERO))
	{
		p->padding = p->min_lenght - (min < 6 ? min : 6);
		pf_padding(p, FALSE);
		write(p->fd, "(null)", (min < 6 ? min : 6));
		p->len += (min < 6 ? min : 6);
		pf_padding(p, TRUE);
	}
	else if (p->min_lenght)
	{
		write(p->fd, "0", 1);
		p->len += 1;
	}
}

void		pf_putstr(t_printf *p, int min)
{
	unsigned	*s;
	int			len;
	int			aux;

	if (!(s = va_arg(p->ap, unsigned*)))
	{
		pf_null_str(p, min);
		return ;
	}
	else
	{
		aux = (int)ft_strlen((const char *)s) < p->precision ?
				(int)ft_strlen((const char *)s) : p->precision;
		len = (p->precision < 0) ? (int)ft_strlen((const char *)s) : aux;
		p->padding = (p->min_lenght - len) > 0 ? (p->min_lenght - len) : 0;
	}
	pf_padding(p, FALSE);
	write(p->fd, s, len);
	p->len += len;
	pf_padding(p, TRUE);
}

void		pf_double_percent(t_printf *p)
{
	p->padding = p->min_lenght - 1;
	pf_padding(p, FALSE);
	write(p->fd, "%", 1);
	pf_padding(p, TRUE);
	p->len++;
}
