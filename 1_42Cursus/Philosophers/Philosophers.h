/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   Philosophers.h                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/15 15:48:49 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/15 16:57:57 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H

#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

# define N				5
# define EATING			0
# define HUNGRY			1
# define THINKING		2
# define LEFT(x)		(x + 4) % N
# define RIGHT(x)		(x + 1) % N

typedef struct			s_data
{
	int state[N];
	int phil[N] = { 0, 1, 2, 3, 4, };
	int i;
}						t_data;

#endif
