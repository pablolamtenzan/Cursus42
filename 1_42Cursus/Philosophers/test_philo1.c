/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   test_philo1.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/15 15:46:27 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/18 13:15:33 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "Philosophers.h"

void			test(t_data data, char *str)
{
	if (data.state[data.phil[data.i]] == HUNGRY
			&& LEFT(data.state[data.phil[data.i]]) != EATING
			&& RIGHT(data.state[data.phil[data.i]] != EATING))
		data.state[data.phil[data.i]] = EATING;
	stop_process(2);
	/*printf philo + forks*/
	// some mutex;
}

void			take_fork(t_data data)
{
	pthread_mutex_lock();
	data.state[data.phil[data.i]] = HUNGRY;
	printf("Philosopher %d is hungry\n", data.phil[data.i] + 1);
	test(data, "both");
	pthread_mutex_unlock(); //?
	pthread_mutex_lock(); // ?
	stop_process(1);
}

void			put_fork(t_data data)
{
	int aux;
	pthread_mutex_lock();
	data.state[data.phil[data.i]] = THINKING;
	printf("Philosopher %d putting fork %d and %d down \n", data.phil[data.i] + 1,
			LEFT(data.phil[data.i]) + 1, data.phil[data.i] + 1);
	printf("Philosopher %d thinking\n" ,data.phil[data.i] + 1);
	aux = data.phil[data.i]; 
	data.phil[data.i] = LETF(data.phil[data.i]);
	test(data);
	aux 
	tets(data);
	pthread_mutex_unlock(); //?
}

void			philosopher(t_data data)
{
	while (42)
	{
	int *aux;

	*aux = *data.phil;

	stop_process(1);//like sleep
	take_fork(data);
	stop_process(0);
	put_fork(data);
	}
}

int				main(int arc, char **argv)
{
	t_data data;
	pthread_t thread_id[N];
	pthread_mutex_t *mutex;
	const pthread_mutexattr_t *attr;

	attr = NULL;
	data.i = -1;
	while (++data.i < N)
	{
		data.phil[data.i] = data.i;
		pthread_create(&thread_id[data.i], NULL, (void *)philosopher, &data);
		printf("Philosopher %d is thinking\n", data.i + 1);
	}
	while (++data.i < N)
		pthread_mutex_init(mutex, attr);
	data.i = -1;
	while (++data.i < N)
		pthread_join(thread_id[data.i], NULL);
}

/*https://www.geeksforgeeks.org/dining-philosopher-problem-using-semaphores/ */
