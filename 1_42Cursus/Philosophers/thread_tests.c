/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   thread_tests.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/15 15:21:10 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/17 08:00:37 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

typedef struct		s_param
{
	int attr1;
	int attr2;
}					parameter_t;

void	*thread_funct(void *parameter)
{
	int *p = (int*)parameter;
	for (int i = 0; i < *p + 1; i++)
	{
		printf("%d of %d\n", i, *p);
		usleep(500000);
	}
	return (0);
}

int main (int arc, char **argv)
{
	pthread_t thread_handle;

	int ret;
	int my_param = 10;

	if ((ret = pthread_create(&thread_handle, 0, thread_funct, (void*)&my_param)) != 0)
	{
		printf("Error:\n %i", ret);
		return (1);
	}
	pthread_join(thread_handle, 0);

	return (0);
}
