/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   double_test.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/26 00:39:48 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/30 17:52:05 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdio.h>

long double		ft_lpow(long double nb, int power)
{
	if (power < 0)
		return (0);
	if (power == 0)
		return (1);
	else
		return (ft_lpow(nb, power - 1) * nb);
}

double pf_get_round(long double nb, int precision)
{
	const long double lpow = ft_lpow(10, precision);
	long double value;
	long double tmp;
	long long  decimal;
	long long  integral;

	value = 0.0000000000000000000000000000000000000001;
	integral = (long long)nb;
	tmp = (nb * lpow);
	decimal = tmp;
	tmp -= decimal;
	if (tmp > 0.5 && precision == 0)
		integral++;
	if ((tmp > 0.5 && (decimal == 0 || decimal % 2)))
	{
		value = integral + (((long double)decimal + 1) / lpow);
		return ((double)value);
	}
	return (nb);
}

int main()
{
	printf("%.25lf", pf_get_round(20.22255555 ,6));
}
