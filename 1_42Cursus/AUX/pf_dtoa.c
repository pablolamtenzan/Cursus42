/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   dtoa.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/23 21:50:47 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/30 17:57:43 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_printf.h"

void		pf_putdouble(t_printf *p)/* there an double fct in bonus in github*/
{
	double	nb;
	intmax_t aux;
	char s[42];

	nb = (double)va_arg(p->ap, double);
	p->precision = p->precision == -1 ? 6 : p->precision;
	p->c == 'f' ? nb = pf_get_round(nb, p->precision) : 0;
	p->c == 'g' ? nb = pf_get_min(nb, p->precision) : 0;
	aux = (intmax_t)nb;
	nb < 0 ? p->f &= ~F_PLUS : 0;
	if (nb <= 0 || p->f & F_PLUS || p->f & F_SPACE)
		++p->printed;
	while (aux /= 10)
		++p->printed;
	p->printed == 0 ? p->printed++ : 0;
	p->printed += p->precision + 1;
	p->padding = MAX(0, p->min_lenght - p->printed);
	!(p->f & F_ZERO) ? pf_padding(p, FALSE) : 0;
	pf_dtoa(p, nb, s, p->precision);
	p->f & F_ZERO ? pf_padding(p, INTEGER_ZERO_PADDING) : 0;
	pf_buffer(p, s, p->printed);
	pf_padding(p, TRUE);
}

int		ft_pow(int nb, int power)
{
	if (power < 0)
		return (0);
	if (power == 0)
		return (1);
	else
		return (ft_pow(nb, power - 1) * nb);
}

void		pf_itoa_double(intmax_t nb, char *s, t_printf *p)
{
	intmax_t aux;
	int len;

	len = 1;
	aux = ABS(nb);
	len = nb <= 0 || p->f & F_PLUS || p->f & F_SPACE ? 1 : 0;
	while (aux /= 10)
		++len;
	nb < 0 ? p->buffer[p->buffer_index++] = '-': 0;
	p->f & F_PLUS ? p->buffer[p->buffer_index++] = '+' : 0;
	p->f & F_SPACE && nb > 0 ? p->buffer[p->buffer_index++] = ' ' : 0;
	aux = ABS(nb);
	while (aux)
	{
		s[--len] = aux % 10 + '0';
		aux /= 10;
	}
}

void		pf_dtoa(t_printf *p, double nb, char *s, int precision)
{
	intmax_t	aux;
	intmax_t	aux_2;
	int			len;

	if (!precision)
	{
		pf_itoa_double((intmax_t)nb, s, p);
		return ;
	}
	aux = (nb * ft_pow(10, precision));
	aux_2 = ABS(aux);
	len = nb <= 0 || p->f & F_PLUS || p->f & F_SPACE ? 3 : 2;
	while (aux /= 10)
		++len;
	nb < 0 ? p->buffer[p->buffer_index++] = '-': 0;
	p->f & F_PLUS ? p->buffer[p->buffer_index++] = '+' : 0;
	p->f & F_SPACE && nb > 0 ? p->buffer[p->buffer_index++] = ' ' : 0;
	while (aux_2)
	{
		s[--len] = (!precision--) ? '.' : aux_2 % 10 + '0';
		aux_2 /= (precision + 1) ? 10 : 1;
	}
}

double		pf_get_round_aux(double res, int precision, double aux, double aux_2)
{
	if (precision == 5 && aux > aux_2)
		res += 0.00001;
	if (precision == 4 && aux > aux_2)
		res += 0.0001;
	if (precision == 3 && aux > aux_2)
		res += 0.001;
	if (precision == 2 && aux > aux_2)
		res += 0.01;
	if (precision == 1 && aux > aux_2)
		res += 0.1;
	return (res);
}

double		pf_get_round(double nb, int precision)
{
	int preci;
	double aux;
	double aux_2;
	double res;

	res = nb;
	preci = precision;
	aux_2 = 0.44;
	while (preci--)
		nb *= 10;
	aux = nb - (intmax_t)nb;
	preci = precision;
	while (preci--)
		aux /= 10;
	preci = precision;
	while (preci--)
		aux_2 /= 10;
	if (precision > 5 && aux > aux_2)
		res += 0.00000011 / ft_pow(10, precision - 6);
	else
		return (pf_get_round_aux(res, precision, aux, aux_2));
	return (res);
}

double		pf_get_min(double nb, int precision)/*only for precison > 2 ? */
{
	int preci;
	double aux;
	double aux_2;
	double res;
	intmax_t res_2;
	float limit[8] = { 0.5509999997 , 0.559999997 , 0.4699999997, 0.4959999997,
		0.4509999997, 0.500999997, 0.551999997, 0.500999997 };

	res = nb;
	res_2 = nb;
	if (precision == 1)
		return (intmax_t)nb;
	preci = precision <= 1 ? 1 : precision - 2;
	aux_2 = limit[precision > 8 ? 8 : precision - 2];
	while (preci--)
		nb *= 10.0000000000000001;
	aux = (nb - (intmax_t)nb) + 0.1 / ft_pow(10, precision - 2);
	preci = precision <= 1 ? 1 : precision - 2;
	while (preci--)
		aux /= 10.0000000000000001;
	preci = precision <= 1 ? 0 : precision - 2;
	while (preci--)
		aux_2 /= 10.0000000000000001;
	if (aux >= aux_2)
	{
		res = (res + (1.0000000000000001) / ft_pow(10, precision - 1) - (aux / 10.0000000000000001));
		if (res_2 == res)
			return (res - 1.0000000000000001);
	}
	if (aux >= aux_2 && res > res_2)
		return (pf_get_min(res, --precision));
	else
		return (res);
}
