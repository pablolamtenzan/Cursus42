/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_dtoa.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/23 23:28:38 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/30 17:52:12 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>
#include <math.h>
#include <stdio.h>

char    *ft_dtoa(double n, int precision)
{
	char    *s;
	long    tmp;
	long    t;
	int        length;

	if (!precision)
		return (ft_itoa((int)n));
	tmp = (n * pow(10, precision));
	t = (tmp < 0) ? -tmp : tmp;
	length = (n <= 0 ? 3 : 2);
	while (tmp && ++length)
		tmp /= 10;
	if (!(s = (char *)malloc(sizeof(char) * length)))
		return (NULL);
	s[--length] = '\0';
	if (tmp <= 0)
		s[0] = (tmp < 0 ? '-' : '0');
	while (t)
	{
		s[--length] = (!precision--) ? '.' : t % 10 + '0';
		t /= (precision + 1) ? 10 : 1;
	}
	return (s);
}
