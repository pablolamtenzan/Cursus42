/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   rt_operations_vectors_tree.c                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/12/04 23:11:53 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/04 23:20:49 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_vector.h"

double		v_len(t_vect a)
{
	return (sqrt((a.x * a.x) + (a.y * a.y) + (a.z * a.z)));
}

double		quad_equ_d(double a, double b, double c)
{
		return ((b * b) -		(4 * a * c));
}

inti		v_quad_equ(double a, double b, double c, t_xy *res)
{
	double	d;
	t_xy	re;

	d = quad_equ_d(a, b, c);
	if (d < 0 || a < 0)
		return (0);
	re.x = (-b + sqrt(d)) / (2.0 * a);
	re.y = (-b - sqrt(d)) / (2.0 * a);
	if (res != NULL)
		*res = re;
	return (1);
}
