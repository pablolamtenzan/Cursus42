/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   mini_RT.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/11 16:16:39 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/03 18:58:30 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

/* DO TO: have to verify the number of elems in ecah line before parse it and if characters are num and ' ' , ',' , '.' */

#include "miniRT.h"

void			miniRT(int arc, char **argv)
{
	t_parse parse;
	t_env env;
	int fd;

	parse.rt = 0;
	env.mlx = mlx_init();
	parse.rt |= RT_D_RES;
	if (!(check_file(arc, argv, &fd, &parse))
			|| (!(search_init_values(&parse, fd))))
		return ;
	else
	{
		close(fd);
		open(argv[1], O_RDONLY);
	}
	env.win = mlx_new_window(env.mlx, parse.res.y, parse.res.x, "The only miniRT in 42Lyon");
	manage_hook(&env);
	parse_elems(fd, &parse, &env);
	parse.rt & RT_SAVE ? ft_save(&parse, &env) : 0;
	mlx_loop(env.mlx);
}


/*save file-> open and create a new fd, and write the string in this window with mlx_pixel_put*/

/* 1st i check the file, i check the file name and then the number of arguments.
 * If the second argument is --save i have to copy the result in a file.with .bmp extension.
 * Then that pre-parse for find the resolution and define the window of the scene.
 * When i have de display window opened i parse the first argument, line per line
 * i display the obs in the window, and... thats the end.*/

/*TO DO:
 * 1) Norme
 * 2) 4 objects
 * 3) Smouth
 * 4) Rotation of obsjects
 * 5) Shadows
 * 6) Save Image
 * 7) Colors RGB->int (there are a mlx fct )
 * 8) 1 arg Window rules: ESC->quit prog ; RedCross->close+quit prog ; Size adaptaion ;
 * 9) Multi-camera change
 * 10) Size render
 * */

/*DONE:
 * 1) Sphere 3D (color isn t right)
 * 2) Parsing
 * 3) Program architecture (80%)
 * 4) Trig and math functions
 * 5) Aux objs functions
 * 6) Wall collision maths
 * 6) Display
 * */
