/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   sphere.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/09 06:23:04 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/10 06:42:43 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"
double map(double x, double in_min, double in_max, double out_min, double out_max)
{
	  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
int			sphere_collisions(t_parse *parse, double *t)
{
	parse->sph_col.pos = parse->eng.ray.pos;
	parse->sph_col.dir = parse->eng.ray.dir;
	parse->sph_col.diff = vec_sub(&parse->sph_col.pos, &parse->sphere.pos);
	parse->sph_col.b = 2 * dot(parse->sph_col.diff, parse->sph_col.dir);
	parse->sph_col.c = dot(parse->sph_col.diff, parse->sph_col.diff) - pow(parse->sphere.d / 2, 2);
	parse->sph_col.disc = pow(parse->sph_col.b, 2) - 4 * parse->sph_col.c;
	if (parse->sph_col.disc < 0)
		return (FALSE);
	else
	{
		parse->sph_col.disc = sqrt(parse->sph_col.disc);
		parse->sph_col.t0 = parse->sph_col.b - parse->sph_col.disc;
		parse->sph_col.t1 = parse->sph_col.b + parse->sph_col.disc;
		*t = (parse->sph_col.t0 < parse->sph_col.t1) ? parse->sph_col.t0 : parse->sph_col.t1;
		return (TRUE);
	}
}

void			put_pixels(t_parse *parse, t_env *env, double t)
{
	t_vector	L;
	t_vector	N;

	parse->eng.aux = vec_scale(&parse->eng.ray.dir, t);
	parse->eng.vec = vec_add(&parse->eng.ray.pos, &parse->eng.aux);
	L = vec_sub(&parse->light.pos, &parse->eng.vec);
	N = get_normal(&parse->eng.vec, &parse->sphere);
	parse->eng.dt = dot(normalize(&L), normalize(&N));
	if (parse->eng.dt < 0.0f)
		parse->eng.dt = 0.0f;
	//parse->eng.dt = map(parse->eng.dt, 0.0, 0.6, 0.0, 0.9);
	
	mlx_pixel_put(env->mlx, env->win, parse->eng.y, parse->eng.x,
		color_from_rgb(parse->sphere.color.r * parse->eng.dt,
			parse->sphere.color.g * parse->eng.dt,
			parse->sphere.color.b * parse->eng.dt));
}

void			put_sphere(t_parse *parse, t_env *env)
{
	double		t;

	parse->eng.x = -1;
	parse->eng.y = -1;
	t = 2000000;
	while (++parse->eng.y < parse->res.y)
	{
		while (++parse->eng.x < parse->res.x)
		{
			parse->eng.ray = ray(vec(parse->eng.x, parse->eng.y, 0), vec(0, 0, 1));
			if (sphere_collisions(parse,  &t))
				put_pixels(parse, env, t);
		}
		parse->eng.x = -1;
	}
}


