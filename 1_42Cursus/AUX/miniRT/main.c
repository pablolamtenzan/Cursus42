/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/05 16:13:53 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/03 16:02:04 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"
#include "get_next_line.h"
int main(int arc, char **argv)
{
	miniRT(arc, argv);
}




/*
int main ()
{
	t_env env;
	t_parse parse;

	parse.res.x = 500;
	parse.res.y = 500;
	//parse.cylinder = rt_cyl(vec(250, 250, 1), vec(0, 0, 1), 100, 100);
	parse.sphere = rt_sphere(vec(250, 250, 50), 400);
	parse.sphere.color.r = 255;
	parse.sphere.color.g = 255;
	parse.sphere.color.b = 0;
	//parse.eng.color = 0x00FF00;
	env.mlx = mlx_init();
	env.win = mlx_new_window(env.mlx, parse.res.y, parse.res.x, "MINIrt");
	put_sphere(&parse, &env);
	//put_cylinder(parse, env);
	mlx_loop(env.mlx);
}
*/
