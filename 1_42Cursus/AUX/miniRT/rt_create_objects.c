/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   create_rt_objects.c                              .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/09 05:00:19 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/08 11:26:11 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

/*add color to params in all fcts*/

t_sphere		rt_sphere(t_vector pos, double d)
{
	t_sphere sphere;

	sphere.pos = pos;
	sphere.d = d;
	return (sphere);
}

t_flat			rt_flat(t_vector pos, t_vector dir)
{
	t_flat flat;

	flat.pos = pos;
	flat.dir = dir;
	return (flat);
}

t_square		rt_square(t_vector pos, t_vector dir, double height)
{
	t_square square;

	square.pos = pos;
	square.dir = dir;
	square.height = height;
	return (square);
}

t_cyl				rt_cyl(t_vector pos, t_vector dir, double d, double height)
{
	t_cyl cyl;

	cyl.pos = pos;
	cyl.dir = dir;
	cyl.d = d;
	cyl.height = height;
	return (cyl);
}

t_tri				rt_tri(t_vector v1, t_vector v2, t_vector v3)
{
	t_tri tri;

	tri.v0 = v1;
	tri.v1 = v2;
	tri.v2 = v3;
	return (tri);
}
