/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   parsing_ret_obs.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/10 08:28:19 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/05 06:50:56 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

int		sphere_parse(char *line, t_parse *parse)
{
	parse->rt |= RT_SPH;
	parse->sphere.pos.x = d_elem(line, 1);
	parse->sphere.pos.y = d_elem(line, 2);
	parse->sphere.pos.z = d_elem(line, 3);
	if (!((parse->sphere.d = d_elem(line, 4)) > 0))
		return (FALSE);
	if (!(((parse->sphere.color.r = i_elem(line, 5)) <= 255)
				&& (i_elem(line, 5)) >= 0))
		return (FALSE);
	if (!(((parse->sphere.color.g = i_elem(line, 6)) <= 255)
				&& (i_elem(line, 6)) >= 0))
		return (FALSE);
	if (!(((parse->sphere.color.b = i_elem(line, 7)) <= 255)
				&& (i_elem(line, 7)) >= 0))
		return (FALSE);
	return (TRUE);
}

int		flat_parse(char *line, t_parse *parse)
{
	parse->rt |= RT_FLA;
	parse->flat.pos.x = d_elem(line, 1);
	parse->flat.pos.y = d_elem(line, 2);
	parse->flat.pos.z = d_elem(line, 3);
	if (!(((parse->flat.dir.x = d_elem(line, 4)) <= 1) && (d_elem(line, 4)) >= -1))
		return (FALSE);
	if (!(((parse->flat.dir.x = d_elem(line, 5)) <= 1) && (d_elem(line, 5)) >= -1))
		return (FALSE);
	if (!(((parse->flat.dir.x = d_elem(line, 6)) <= 1) && (d_elem(line, 6)) >= -1))
		return (FALSE);
	if (!(((parse->flat.color.r = i_elem(line, 7)) <= 255) && (i_elem(line, 7)) >= 0))
			return (FALSE);
	if (!(((parse->flat.color.g = i_elem(line, 8)) <= 255) && (i_elem(line, 8)) >= 0))
		return (FALSE);
	if (!(((parse->flat.color.b = i_elem(line, 9)) <= 255) && (i_elem(line, 8)) >= 0))
		return (FALSE);
	return (TRUE);
}

int		square_parse(char *line, t_parse *parse)
{
	parse->rt |= RT_SQU;
	parse->square.pos.x = d_elem(line, 1);
	parse->square.pos.y = d_elem(line, 2);
	parse->square.pos.z = d_elem(line, 3);
	if (!(((parse->square.dir.x = d_elem(line, 4)) <= 1) && (d_elem(line, 4)) >= -1))
		return (FALSE);
	if (!(((parse->square.dir.y = d_elem(line, 5)) <= 1) && (d_elem(line, 5)) >= -1))
		return (FALSE);
	if (!(((parse->square.dir.z = d_elem(line, 6)) <= 1) && (d_elem(line, 6)) >= -1))
		return (FALSE);
	if (!((parse->square.height = d_elem(line, 7)) > 0))
		return (FALSE);
	if (!(((parse->square.color.r = i_elem(line, 8)) <= 255)
				&& (i_elem(line, 8)) >= 0))
		return (FALSE);
	if (!(((parse->square.color.g = i_elem(line, 9)) <= 255)
				&& (i_elem(line, 9)) >= 0))
		return (FALSE);
	if (!(((parse->square.color.b = i_elem(line, 10)) <= 255)
				&&
				(i_elem(line, 10)) >= 0))
		return (FALSE);
	return (TRUE);
}

int		cyl_parse(char *line, t_parse *parse)
{
	parse->rt |= RT_CYL;
	parse->cylinder.pos.x = d_elem(line, 1);
	parse->cylinder.pos.y = d_elem(line, 2);
	parse->cylinder.pos.z = d_elem(line, 3);
	if(!(((parse->cylinder.dir.x = d_elem(line, 4)) <= 1) && (d_elem(line, 4)) >= -1))
		return (FALSE);
	if(!(((parse->cylinder.dir.y = d_elem(line, 5)) <= 1) && (d_elem(line, 5)) >= -1))
		return (FALSE);
	if (!((parse->cylinder.dir.z = d_elem(line, 6) <= 1) && (d_elem(line, 6)) >= -1))
		return (FALSE);
	if (!((parse->cylinder.d = d_elem(line, 7)) > 0))
		return (FALSE);
	if (!((parse->cylinder.height = d_elem(line, 8)) > 0))
		return (FALSE);
	if (!(((parse->cylinder.color.r = i_elem(line, 9)) <= 255) &&
				i_elem(line, 9) >= 0))
		return (FALSE);
	if (!(((parse->cylinder.color.g = i_elem(line, 10)) <= 255) &&
				(i_elem(line, 10)) >= 0))
		return (FALSE);
	if (!(((parse->cylinder.color.b = i_elem(line, 11)) <= 255) &&
				(i_elem(line, 11)) >= 0))
		return (FALSE);
	return (TRUE);
}

int		tri_parse(char *line, t_parse *parse)
{
	parse->rt |= RT_TRI;
	parse->triangle.v0.x = d_elem(line, 1);
	parse->triangle.v0.y = d_elem(line, 2);
	parse->triangle.v0.z = d_elem(line, 3);
	parse->triangle.v1.x = d_elem(line, 4);
	parse->triangle.v1.y = d_elem(line, 5);
	parse->triangle.v1.z = d_elem(line, 6);
	parse->triangle.v2.x = d_elem(line, 7);
	parse->triangle.v2.y = d_elem(line, 8);
	parse->triangle.v2.z = d_elem(line, 9);
	if (!(((parse->triangle.color.r = i_elem(line, 10)) <= 255) &&
				(i_elem(line, 10)) >= 0))
		return (FALSE);
	if (!(((parse->triangle.color.g = i_elem(line, 11)) <= 255) &&
				(i_elem(line, 11)) >= 0))
		return (FALSE);
	if (!(((parse->triangle.color.b = i_elem(line, 12)) <= 255) &&
				(i_elem(line, 12)) >= 0))
		return (FALSE);
	return (TRUE);
}
