/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   put_square.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 00:11:35 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/07 01:41:04 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

int		put_square(t_square square, t_env env)
{
	int		hx;
	int		hy;

	hx = 0;
	hy = 0;
	while (hx < square.height)
	{
		hy = 0;
		while (hy < square.height)
		{
			mlx_pixel_put(env.mlx, env.win, square.pos.x - square.height / 2 + hx, square.pos.y - square.height / 2 + hy, 0xFFFFFF);
			hy++;
		}
		hx++;
	}
	return (0);
}
