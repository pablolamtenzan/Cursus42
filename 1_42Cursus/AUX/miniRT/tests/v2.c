/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   v2.c                                             .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/08 01:26:29 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/10 01:14:26 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

t_vector	vec(double x, double y, double z)
{
	t_vector vec;
	vec.x = x;
	vec.y = y;
	vec.z = z;
	return (vec);
}

t_vector	vec_sub(t_vector *v1, t_vector *v2)
{
	t_vector res;

	res.x = v1->x - v2->x;
	res.y = v1->y - v2->y;
	res.z = v1->z - v2->z;
	return (res);
}

t_vector	vec_add(t_vector *v1, t_vector *v2)
{
	t_vector res;

	res.x = v1->x + v2->x;
	res.y = v1->y + v2->y;
	res.z = v1->z + v2->z;
	return (res);
}

t_vector	vec_scale(t_vector *v, double c)
{
	t_vector res;

	res.x = v->x * c;
	res.y = v->y * c;
	res.z = v->z * c;
	return (res);
}

t_vector	vec_div(t_vector *v1, double d)
{
	t_vector res;

	res.x = v1->x / d;
	res.y = v1->y / d;
	res.z = v1->z / d;


	return (res);
}

t_ray		ray(t_vector pos, t_vector dir)
{
	t_ray ray;
	ray.pos = pos; //origin
	ray.dir = dir; //direction
	return (ray);
}

t_vector	get_normal(t_vector *pi, t_sphere *sphere)
{

	t_vector sub = vec_sub(&sphere->pos , pi);
	t_vector div = vec_div(&sub, sphere->d / 2);
	return (div);
}

t_sphere	sphere(t_vector center, double rad)
{
	t_sphere	sphere;
	
	sphere.pos = center;
	sphere.d = rad * 2;
	return (sphere);
}

t_vector	normalize(t_vector *v)
{
	double mg;

	mg = sqrt(pow(v->x, 2) + pow(v->y, 2) + pow(v->z, 2));
	return (vec_div(v, mg));
}

double		dot(t_vector v, t_vector b)
{
	return ((v.x * b.x) + (v.y * b.y) + (v.z * b.z));
}

double			sphere_intersection(t_ray *ray, t_sphere *sphere, double *t)
{
	t_vector	pos;
	t_vector	dir;
	t_vector	len;
	float		b;
	float		c;
	float		disc;
	float		t0;
	float		t1;

	pos = ray->pos;
	dir = ray->dir;
	len = vec_sub(&pos, &sphere->pos);
	b = 2 * dot(len, dir);
	c = dot(len, len) - pow(sphere->d / 2, 2);
	disc = pow(b , 2) - 4 * c;
	if (disc < 0)
		return (FALSE);
	else
	{
		disc = sqrt(disc);
		t0 = - b - disc;
		t1 = - b + disc;
		*t = (t0 < t1) ? t0 : t1;
		return (TRUE);
	}
}

t_color		color(float r, float g, float b)
{
	t_color color;

	color.r = r;
	color.g = g;
	color.b = b;
	return (color);
}

int			main ()
{
	t_env env;
	t_Res resolution;
	t_ray rayy;

	t_color white = color(255, 255, 255);
	//int white = 0x00FFFFFF;
	const int W = 500;
	const int H = 500;
	t_color pixel_col[H][W];
	t_sphere light = sphere(vec(0, 0, 50), 200);
	t_sphere spher = sphere(vec( W / 2, H / 2, 50), 200);
	int y = 0;
	int x = 0;
	double t = 20000;
	env.mlx = mlx_init();
	env.win = mlx_new_window(env.mlx, W, H, "V2");
	double i = 0.000000005;

	while (++y < H) //for each pixel
	{
		while (++x < W)
		{
			//send a tay to each pixel
			rayy = ray(vec(x, y, 0), vec(0, 0, 1));
			//check for intersections
			if ((sphere_intersection(&rayy, &spher, &t)))/*return t*/
			{
				// point of intersections
				t_vector scale = vec_scale(&rayy.dir, t);
				t_vector pi = vec_add(&rayy.pos, &scale);
				// color pixel
				t_vector L = vec_sub(&light.pos, &pi);
				t_vector N = get_normal(&pi, &spher);
				printf("------------------------------\n");
				printf("L.x = %f\n N.x = %f\n\n", L.x, N.x);
				printf("L.y = %f\n N.y = %f\n\n", L.y, N.y);
				printf("L.z = %f\n N.z = %f\n\n", L.z, N.z);

				double dt = dot(normalize(&L), normalize(&N));
				printf("DT = %.*f\n\n",15,  dt);
				mlx_pixel_put(env.mlx, env.win, y, x, 16777215 * dt);
				/* now i have a non constant dt so i have to replace 
				 * mlx_pixel_put for another func for modify ary rgb, afetr smoking*/
				i += 0.0000000003;
			}
		}
		x = 0;
	}
	mlx_loop(env.mlx);
}

/* https://www.youtube.com/watch?v=ARn_yhgk7aE */

