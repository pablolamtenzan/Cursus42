/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   engine.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/07 04:09:43 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/09 07:11:10 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

static	t_ray		rt_colision(t_ray *ray, t_line *line)
{
	t_collision coll;

	coll.x1 = ray.pt.x;
	coll.y1 = ray.pt.y;
	coll.x2 = ray.pt.x + ray.dir.x;
	coll.y2 = ray.pt.y + ray.dir.y;
	coll.den = (line.start.x - line.end.x) * (coll.y1 - coll.y2) -
			(line.start.y - line.end.y) * (coll.x1 - coll - x2);
	if (coll.den == 0)
		return (FALSE);
	coll.t = ((line.start.x - coll.x1) * (coll.y1 - coll.y2) -
			(line.start.y - coll.y1) * (coll.x1 - coll.x2) / coll.den);
	coll.u = - ((line.start.x - line.end.x) * (line.start.y - coll.y1)
			- (line.start.y - line.end.y) * (line.start.x - coll.x1) / coll.den);
	if (coll.t > 0 && coll.t < 1 && coll.u > 0)
	{
		ray.pt.x = line.start.x + coll.t * (line.end.x - line.start.x);
		ray.pt.y = line.start.y + coll.t * (line.end.y - line.start.y);
		return (ray);
	}
	return (FALSE);
}

float				rt_distance(t_cam *cam, t_line *line)
{
	short i;
	float dis;
	t_ray ray;
	t_ray coll;

	i = 0;
	while (i < cam.vision_field)
		ray = new_ray(t_cam pos, convert_rad(i));
		if (coll = rt_colision(ray, line))
		{
			dis = sqrt(pow(coll.pt.x - cam.pos.x) + pow(coll.pt.y - cam.pos.y));
			rt_function_who_does_something_with_distance();
			i += 2;
		}
}

t_ray new_ray(t_cam origin, float dir)
{

/*xb - xa + yb - ya */
