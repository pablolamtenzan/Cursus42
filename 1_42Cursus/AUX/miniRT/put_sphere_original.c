/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   sphere.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/09 06:23:04 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/10 07:32:25 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

static double			sphere_collisions(t_ray *ray, t_sphere *sphere,
		double *t)
{
	t_sph_col sph_col;

	sph_col.pos = ray->pos;
	sph_col.dir = ray->dir;
	sph_col.diff = vec_sub(&sph_col.pos, &sphere->pos);
	sph_col.b = 2 * dot(sph_col.diff, sph_col.dir);
	sph_col.c = dot(sph_col.diff, sph_col.diff) - pow(sphere->d / 2, 2);
	sph_col.disc = pow(sph_col.b, 2) - 4 * sph_col.c;
	if (sph_col.disc < 0)
		return (FALSE);
	else
	{
		sph_col.disc = sqrt(sph_col.disc);
		sph_col.t0 = sph_col.b - sph_col.disc;
		sph_col.t1 = sph_col.b + sph_col.disc;
		*t = (sph_col.t0 < sph_col.t1) ? sph_col.t0 : sph_col.t1;
		return (TRUE);
	}
}
/* have to check posible errors about maths algos*/

int				color_limit(int color)
{
	if (color > 0x00FFFFFF)
		return (0x00FFFFFF);
	else if (color < 0x00000000)
		return (0x00000000);
	else
		return (color);
}

//void			put_sphere(t_parse *parse, t_env *env)
{
	/*must group all inpout values in a struct and put it in params*/
	t_vector	L;
	t_vector	N;
	double		t;

	t_sphere light = rt_sphere(vec(250, 250, 50), 200);
	/*params depend of light input but it isn t implement*/
	parse->eng.x = -1;
	parse->eng.y = -1;
	t = 2000000;
	while (++parse->eng.y < parse->res.y)
	{
		while (++parse->eng.x < parse->res.x)/*for each pixel */
		{
			parse->eng.ray = ray(vec(parse->eng.x, parse->eng.y, 0), vec(0, 0, 1));/*send ray to each pixel */
			if (sphere_collisions(&parse->eng.ray, &parse->sphere, &t))
			{
				parse->eng.aux = vec_scale(&parse->eng.ray.dir, t);
				parse->eng.vec = vec_add(&parse->eng.ray.pos, &parse->eng.aux);
				L = vec_sub(&parse->light.pos, &parse->eng.vec);
				N = get_normal(&parse->eng.vec, &parse->sphere);
				parse->eng.dt = dot(normalize(&L), normalize(&N));
				if (parse->eng.dt < 0.0f)
					parse->eng.dt = 0.0f;
				mlx_pixel_put(env->mlx, env->win, parse->eng.y, parse->eng.x,
						color_from_rgb(222 * parse->eng.dt, 100 * parse->eng.dt,
							22 * parse->eng.dt));
			}
		}
		parse->eng.x = -1;
	}
}
