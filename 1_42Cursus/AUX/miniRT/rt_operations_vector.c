/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_operations_vector.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/09 04:49:10 by plamtenz          #+#    #+#             */
/*   Updated: 2019/12/17 19:48:07 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "miniRT.h"

double		dot(t_vector v1, t_vector v2)
{
	return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
}

t_vector	get_normal(t_vector *v, t_sphere *sphere)
{
	t_vector sub;

	sub = vec_sub(&sphere->pos, v);
	return (vec_div(&sub, sphere->d / 2));
}

t_vector	normalize(t_vector *v)
{
	double md;

	md = sqrt(pow(v->x, 2) + pow(v->y, 2) + pow(v->z, 2));
	return (vec_div(v, md));
}

t_vector	cross_product(t_vector v1, t_vector v2)
{
	t_vector res;
	
	res.x = v1.y * v2.z - v1.z * v2.y;
	res.y = v1.z * v2.x - v1.x * v2.z;
	res.z = v1.x * v2.y - v1.y * v2.x;
	return (res);
}

