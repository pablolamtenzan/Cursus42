/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   miniRT.h                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/04 21:45:00 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/04 23:29:43 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef MINIRT_H
# define MINIRT_H

#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <math.h>

#include "mlx.h"
#include "mlx_int.h"

# define TRUE				1
# define FALSE				0

# define RT_RES				(1 << 0)
# define RT_AMB				(1 << 1)
# define RT_CAM				(1 << 2)
# define RT_LIG				(1 << 3)
# define RT_SPH				(1 << 4)
# define RT_FLA				(1 << 5)
# define RT_SQU				(1 << 6)
# define RT_CYL				(1 << 7)
# define RT_TRI				(1 << 8)
# define RT_SAVE			(1 << 9)
# define RT_D_RES			(1 << 10)
# define RT_D_AMB			(1 << 11)

typedef unsigned long		t_size;

typedef struct				s_color
{
	int						r;
	int						g;
	int						b;
}							t_color;

typedef struct				s_vector
{
	double						x;
	double						y;
	double						z;
}							t_vector;

typedef struct				s_ray
{
	t_vector				pos;
	t_vector				dir;
}							t_ray;

typedef struct				s_Res
{
	float					x;
	float					y;
}							t_Res;

typedef struct				s_Amb
{
	float					amb_l;
	t_color					color;
}							t_Amb;

typedef struct				s_cam
{
	t_vector				pos;
	t_vector				dir;
	int						vision_field;
}							t_cam;

typedef struct				s_light
{
	t_vector				pos;
	float					lum_rat;
	t_color					color;
}							t_light;

typedef struct				s_sphere
{
	t_vector				pos;
	double					d;
	t_color					color;
}							t_sphere;

typedef struct				s_flat
{
	t_vector				pos;
	t_vector				dir;
	t_color					color;
}							t_flat;

typedef struct				s_square
{
	t_vector				pos;
	t_vector				dir;
	double					height;
	t_color					color;
}							t_square;

typedef struct				s_cyl
{
	t_vector				pos;
	t_vector				dir;
	double					d;
	double					height;
	t_color					color;
}							t_cyl;

typedef struct				s_tri
{
	t_vector				v0;
	t_vector				v1;
	t_vector				v2;
	t_color					color;
}							t_tri;

typedef struct				s_env
{
	void					*mlx;
	void					*win;
}							t_env;

typedef struct				s_sph_col
{
	t_vector				pos;
	t_vector				dir;
	t_vector				diff;
	double					b;
	double					c;
	double					disc;
	float					t0;
	float					t1;
}							t_sph_col;

typedef struct				s_engine
{
	t_ray					ray;
	int						color;
	int						x;
	int						y;
	double					dt;
	t_vector				aux;
	t_vector				vec;
	t_vector				v0v1;
	t_vector				v0v2;
	t_vector				pvec;
	t_vector				tvec;
	t_vector				qvec;
	double					det;
	double					u;
	double					v;
}							t_engine;

typedef struct				s_parse
{
	int						rt;
	t_engine				eng;
	t_Res					res;
	t_Amb					amb;
	t_cam					cam;
	t_light					light;
	t_flat					flat;
	t_sphere				sphere;
	t_sph_col				sph_col;
	t_square				square;
	t_cyl					cylinder;
	t_tri					triangle;
}							t_parse;

/*PARSING OBJS*/
t_vector					vec(double x, double y, double z);
t_ray						ray(t_vector pos, t_vector dir);
t_color						color(int r, int g, int b);

/*RT OBJS*/
t_sphere					rt_sphere(t_vector pos, double d);
t_flat						rt_flat(t_vector pos, t_vector dir);
t_square					rt_square(t_vector pos, t_vector dir, double height);
t_cyl						rt_cyl(t_vector pos, t_vector dir, double d, double height);
t_tri						rt_tri(t_vector v1, t_vector v2, t_vector v3);

/*TRIG OPERATIONS*/
double						dot(t_vector v1, t_vector v2);
t_vector					get_normal(t_vector *v, t_sphere *sphere);
t_vector					normalize(t_vector *v);
t_vector					cross_product(t_vector v1, t_vector v2);

/*VECTOR OPERATIONS*/
t_vector					vec_sub(t_vector *v1, t_vector *v2);
t_vector					vec_add(t_vector *v1, t_vector *v2);
t_vector					vec_scale(t_vector *v, double c);
t_vector					vec_div(t_vector *v, double c);

/*OBJS DISPLAY FUNCTIONS*/
void						put_sphere(t_parse *parse, t_env *env);
int							sphere_collisions(t_parse *parse, double *t);
void						put_cylinder(t_parse *parse, t_env *env);
char						plane_intersections(t_parse parse, t_ray ray,
		double *t);
char						square_intersections(t_parse *parse, t_ray ray,
		double *t);
char						moller_trumbore(t_parse *parse, t_tri tri,
		t_ray ray, double *t);
void						set_resolution(int x, int y, t_env *env);
void						set_ambiental(t_parse *parse, t_env *env);
void						set_ambiental_light(t_parse *parse, t_env *env);
void						put_flat(t_parse *parse, t_env *env);
void						put_square(t_parse *parse, t_env *env);
void						put_triangle(t_parse *parse, t_env *env);
void						put_camera(t_parse *parse, t_env *env);
void						put_light(t_parse *parse, t_env *env);

/*PARSING FUNCTIONS*/
int							check_file_name(char *fn);
int							check_file(int arc, char **argv, int *fd,
		t_parse *parse);
int							parse_line(char *line, t_parse *parse, char *start);
void						parse_elems(int fd, t_parse *parse, t_env *env);
int							res_parse(char *line, t_parse *parse);
int							amb_parse(char *line, t_parse *parse);
int							cam_parse(char *line, t_parse *parse);
int							light_parse(char *line, t_parse *parse);
int							sphere_parse(char *line, t_parse *parse);
int							flat_parse(char *line, t_parse *parse);
int							square_parse(char *line, t_parse *parse);
int							cyl_parse(char *line, t_parse *parse);
int							tri_parse(char *line, t_parse *parse);

/*DISPLAY ORGANIZATION FCT*/
int							search_init_values(t_parse *parse, int fd);
void						put_elem_in_scene(t_parse *parse, t_env *env);

/*AUXILIAR FNCT*/
int							ft_error(char *error);
int							i_elem(char *line, int it);
double						d_elem(char *line, int it);
double						ft_atod(char *str);
int							ft_atoi(const char *str);
int							ft_isdigit(int c);
int							ft_strlen(const char *str);
unsigned int				color_from_rgb(unsigned char r, unsigned char g,
		unsigned char b);
void						color_get_rgb(unsigned int rgb, unsigned char *r,
		unsigned char *g, unsigned char *b);
char						**split_multicharset(char *str, char *charset);
int							ft_strncmp(const char * s1, const char *s2,
		t_size n);

/*MINIRT*/
void						miniRT(int arc, char **argv);

/*more TO DO */
void						ft_save(t_parse *parse, t_env *env);
void						manage_hook(t_env *env);




/* https://github.com/vnguyen42/rtv1/blob/master/ft_rtv1.h */

#endif
