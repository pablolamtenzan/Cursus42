/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   parsing_ret.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/10 07:50:52 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/03 20:14:23 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"
int			res_parse(char *line, t_parse *parse)
{
	parse->rt & RT_D_RES ? parse->rt &= ~RT_D_RES : 0;
	parse->rt |= RT_RES;
	if (!((parse->res.y = i_elem(line, 1)) > 0)) /*DO PARENTHESES LIKE THAT in all fct*/
		return (FALSE);
	if (!((parse->res.x = i_elem(line, 2)) > 0))
		return (FALSE);
	return (TRUE);
}

int		amb_parse(char *line, t_parse *parse)
{
	parse->rt |= RT_AMB;
	if (parse->rt & RT_D_AMB)
		ft_error("Error:\nAmbiental light can't be declared twice\n");
	else
		parse->rt |= RT_D_AMB;
	if (!(((parse->amb.amb_l = d_elem(line, 1)) <= 1) && (d_elem(line, 1)) >= 0))
		return (FALSE);
	if (!(((parse->amb.color.r = i_elem(line, 2)) <= 255) &&
				(i_elem(line, 2)) >= 0))
		return (FALSE);
	if (!(((parse->amb.color.g = i_elem(line, 3)) <= 255) &&
				(i_elem(line, 3)) >= 0))
		return (FALSE);
	if (!(((parse->amb.color.b = i_elem(line, 4)) <= 255) &&
				(i_elem(line, 4)) >= 0))
		return (FALSE);
	return (TRUE);
}

int		cam_parse(char *line, t_parse *parse)
{
	parse->rt |= RT_CAM;
	parse->cam.pos.x = d_elem(line, 1);
	parse->cam.pos.y = d_elem(line, 2);
	parse->cam.pos.z = d_elem(line, 3);
	if (!(((parse->cam.dir.x = d_elem(line, 4)) <= 1) && (d_elem(line, 4)) >= -1))
		return (FALSE);
	if (!(((parse->cam.dir.y = d_elem(line, 5)) <= 1) && (d_elem(line, 5)) >= -1))
		return (FALSE);
	if (!(((parse->cam.dir.z = d_elem(line, 6)) <= 1) && (d_elem(line, 6)) >= -1))
		return (FALSE);
	if (!((parse->cam.vision_field = i_elem(line, 7)) > 0))
		return (FALSE);
	return (TRUE);
}

int		light_parse(char *line, t_parse *parse)
{
	parse->rt |= RT_LIG;
	parse->light.pos.x = d_elem(line, 1);
	parse->light.pos.y = d_elem(line, 2);
	parse->light.pos.z = d_elem(line, 3);
	if (!(((parse->light.lum_rat = d_elem(line, 4)) <= 1) && (d_elem(line, 4)) >= 0))
		return (FALSE);
	if (!(((parse->light.color.r = i_elem(line, 5)) <= 255) && (i_elem(line, 5)) >= 0))
		return (FALSE);
	if (!(((parse->light.color.g = i_elem(line, 6)) <= 255) && (i_elem(line, 5)) >= 0))
		return (FALSE);
	if (!(((parse->light.color.b = i_elem(line, 7)) <= 255) && (i_elem(line, 5)) >= 0))
		return (FALSE);
	return (TRUE);
}
