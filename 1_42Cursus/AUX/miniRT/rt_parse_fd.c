/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   rt_parse_fd.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/28 06:53:16 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/03 20:22:07 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"
#include "get_next_line.h"

int			stack_in_struct(char *info, t_parse *parse) /*put values in struct + check if this values are ok*/
{
	while (*info == ' ')
		info++;
	if (*info == 'R' && *(info + 1) == ' ')
		return (res_parse(info, parse) != FALSE ? 
				res_parse(info, parse) : FALSE);
	if (*info == 'A' && *(info + 1) == ' ')
		return (amb_parse(info, parse) ?
				amb_parse(info, parse) : FALSE);
	else if (*info == 'c' && *(info + 1) == ' ')
		return (cam_parse(info, parse) ? cam_parse(info, parse) : FALSE);
	else if (*info == 'l' && *(info + 1) == ' ')
		return (light_parse(info, parse) ? light_parse(info, parse) : FALSE);
	else if (*info == 'p' && *(info + 1) == 'l' && *(info + 2) == ' ')
		return (flat_parse(info, parse) ? flat_parse(info, parse) : FALSE);
	else if (*info == 's' && *(info + 1) == 'p' && *(info + 2) == ' ')
		return (sphere_parse(info, parse) ? sphere_parse(info, parse) : FALSE);
	else if (*info == 's' && *(info + 1) == 'q' && *(info + 2) == ' ')
		return (square_parse(info, parse) ? square_parse(info, parse) : FALSE);
	else if (*info == 'c' && *(info + 1) == 'y' && *(info + 2) == ' ')
		return (cyl_parse(info, parse) ? cyl_parse(info, parse) : FALSE);
	else if (*info == 't' && *(info + 1) == 'r' && *(info + 2) == ' ')
		return (tri_parse(info, parse) ? tri_parse(info, parse) : FALSE);
	else
		return (FALSE);
	return (TRUE);
}

int			parse_line(char *line, t_parse *parse, char *start) /*return the elem parsed in the struct */
{
	if (!(stack_in_struct(line, parse)))
		return (FALSE);
	free(start);
	return (TRUE);
}

void			parse_elems(int fd, t_parse *parse, t_env *env) /*take each of the imput and parses it*/
{
	char		*line;
	char		*start;
	int			ret_gnl;
	if (fd)
	{
		parse->rt |= RT_D_RES;
		while ((ret_gnl = get_next_line(fd, &line)) > 0)
		{
			start = line;
			//(!(parse->rt & RT_D_RES)) ? ft_error("Error:\nBad Imput\n") : 0;
			if (!(parse_line(line, parse, start)) && ret_gnl != 1)
				ft_error("Error:\nNo valid file\n");
			start = NULL;
			put_elem_in_scene(parse, env);
		}
	}
	else
		ft_error("Error:\nNo file found\n");
}
