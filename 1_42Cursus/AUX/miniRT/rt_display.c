/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   display.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/11 12:09:42 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/02 21:37:37 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

static void		set_const_objs(t_parse *parse, t_env *env) /*the bitwise isn t reinitialisated so can only be defined 1 time*/
{
	if (parse->rt & RT_RES)
	{
		set_resolution(parse->res.x , parse->res.y, env);
		parse->rt &= ~RT_RES;
	}
	else if (parse->rt & RT_AMB)
	{
		set_ambiental_light(parse, env);
		parse->rt &= ~RT_AMB;
	}
}

void			put_elem_in_scene(t_parse *parse, t_env *env) /*displays elems in the scene using the parse */
{
	if (parse->rt & RT_RES) /*|| parse->rt & RT_AMB)*/
		set_const_objs(parse, env);
	else if (parse->rt & RT_SPH)
		put_sphere(parse, env);
	else if (parse->rt & RT_FLA)
		put_flat(parse, env);
	else if (parse->rt & RT_SQU)
		put_square(parse, env);
	else if (parse->rt & RT_CYL)
		put_cylinder(parse, env);
	else if (parse->rt & RT_TRI)
		put_triangle(parse, env);
	else if (parse->rt & RT_LIG)
		put_light(parse, env);
	else if (parse->rt & RT_CAM)
		put_camera(parse, env);
	parse->rt &= ~RT_SPH;
	parse->rt &= ~RT_FLA;
	parse->rt &= ~RT_SQU;
	parse->rt &= ~RT_CYL;
	parse->rt &= ~RT_TRI;
	parse->rt &= ~RT_LIG;
	parse->rt &= ~RT_CAM;
}
