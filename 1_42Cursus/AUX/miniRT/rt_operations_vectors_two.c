/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   vector_operations.c                              .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/09 04:41:13 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/04 23:11:09 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

t_vector	vec_sub(t_vector *v1, t_vector *v2)
{
	t_vector res;

	res.x = v1->x - v2->x;
	res.y = v1->y - v2->y;
	res.z = v1->z - v2->z;
	return (res);
}

t_vector	vec_add(t_vector *v1, t_vector *v2)
{
	t_vector res;

	res.x = v1->x + v2->x;
	res.y = v1->y + v2->y;
	res.z = v1->z + v2->z;
	return (res);
}

t_vector	vec_scale(t_vector *v, double c)
{
	t_vector res;

	res.x = v->x * c;
	res.y = v->y * c;
	res.z = v->z * c;
	return (res);
}

t_vector	vec_div(t_vector *v, double c)
{
	t_vector res;

	res.x = v->x / c;
	res.y = v->y / c;
	res.z = v->z / c;
	return (res);
}
