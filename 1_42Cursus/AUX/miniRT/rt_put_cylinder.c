/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   cylinder.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/13 21:51:45 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/03 23:08:51 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

/* https://www.cl.cam.ac.uk/teaching/1999/AGraphHCI/SMAG/node2.html#SECTION00023200000000000000 */

#include "miniRT.h"

t_vector		normal_in(t_vector *p, t_cyl cyl)
{
	if (p->x < cyl.pos.x + cyl.d / 2 && p->x > cyl.pos.x - cyl.d / 2 - cyl.d / 2 && p->z < cyl.pos.z - cyl.d / 2)
	{
		double epsilon = 0.0000001;
		if (p->y < cyl.pos.y + cyl.height + epsilon && p->y > cyl.pos.y - epsilon)
			return (vec(0, 1, 0));
		if (p->y < cyl.pos.y + epsilon && p->y > cyl.pos.y - epsilon)
			return (vec(0, -1, 0));
	}
	t_vector c0;
	c0 = vec(cyl.pos.x, p->y, cyl.pos.z);
	t_vector v = vec_sub(p, &c0);
	normalize(&v);
	return (v);
}

char		base_intersection(t_cyl cyl, t_vector *c, t_ray ray,  double *t)
{
	t_vector normal = normal_in(c, cyl);
	t_vector p0 = vec_sub(&ray.pos, c);
	double a = normal.x;
	double b = normal.y;
	double C = normal.z;
	double d = - (a*(cyl.pos.x - c->x) + b * (cyl.pos.y - c->y) + C * (cyl.pos.z - c->z));
	if (a * ray.dir.x + b * ray.dir.y + C * ray.dir.z == 0)
		return (0);
	double dist = - (a * p0.x + b *p0.y + C * p0.z + d) / (a * ray.dir.x + b *ray.dir.y + C * ray.dir.z);
	double epsilon = 0.00000001;
	if (dist < epsilon)
		return (0);
	t_vector p;
	p.x = p0.x + dist * ray.dir.x;
	p.y = p0.y + dist * ray.dir.y;
	p.z = p0.z + dist * ray.dir.z;
	if (pow(p.x, 2) + pow(p.z, 2) - pow(cyl.d / 2, 2) > epsilon)
		return (0);
	*t = dist;
	return (1);
}

char		cylinder_collision(t_ray ray, t_cyl cyl, t_env env, double *t)
{//will be t_parsin after
	double a;
	double b;
	double c;
	double delta;
	double epsilon;
	double y;
	double dist;
	/*translation of origin ray*/
	t_vector p0 = vec_sub(&ray.pos, &cyl.pos);
	a = pow(ray.dir.x, 2) + pow(ray.dir.z, 2);
	b = ray.dir.x * p0.x + ray.dir.z * p0.z;
	c = pow(p0.x, 2) + pow(p0.z, 2) - pow(cyl.d / 2, 2);
	delta = pow(b, 2) - a * c;
	/*epsilon exist cause double cause computing errors*/
	epsilon = 0.00000001;
	if (delta < epsilon)
		return (0);
	/* t = the nearnest intersection */
	*t = (-b - sqrt(delta))/a;
	/* t < 0 means the intersection is behind the ray originigin */
	if (*t <= epsilon)
		return (0);
	y = p0.y + *t * ray.dir.y;
	if (y > cyl.height || y < -epsilon)
	{
	t_vector center2 = vec(cyl.pos.x, cyl.pos.y + cyl.height, cyl.pos.z);
		char b1 = base_intersection(cyl, &center2, ray, &dist);
		if (b1)
			*t = dist;
		char b2 = base_intersection(cyl,&cyl.pos, ray, &dist);
		if (b2 && dist > epsilon && *t >= dist)
			*t = dist;
		return b1 || b2;
	}
	return (1);
}

t_vector	gt_normal(t_vector *v, t_cyl *sphere)
{
	t_vector sub;

	sub = vec_sub(&sphere->pos, v);
	return (vec_div(&sub, sphere->d / 2));
}


void		put_cylinder(t_parse *parse, t_env *env)
{
	t_vector L;
	t_vector N;
	double t;

	//t_sphere light = rt_sphere(vec(250, 250, 50), 200);
	parse->eng.x = -1;
	parse->eng.y = -1;
	t = 2000000;
	while (++parse->eng.y < parse->res.y)
	{
				while (++parse->eng.x < parse->res.x)
		{
			parse->eng.ray = ray(vec(parse->eng.x, parse->eng.y, 0), vec(0, 0, 1));
			if (cylinder_collision(parse->eng.ray, parse->cylinder, *env, &t))
			{
				parse->eng.aux = vec_scale(&parse->eng.ray.dir, t);
				parse->eng.vec = vec_add(&parse->eng.ray.pos, &parse->eng.aux);
				L = vec_sub(&parse->light.pos, &parse->eng.vec);
				N = gt_normal(&parse->eng.vec, &parse->cylinder);
				parse->eng.dt = dot(normalize(&L), normalize(&N));
				if (parse->eng.dt < 0.0f)
					parse->eng.dt = 0.0f;
				printf("%f\n", parse->eng.dt);

				mlx_pixel_put(env->mlx, env->win, parse->eng.y, parse->eng.x, color_from_rgb(255 * parse->eng.dt , 255 * parse->eng.dt, 22 * parse->eng.dt));
			}
		}
		parse->eng.x = -1;
	}
}

/*https://github.com/spinatelli/raytracer/blob/master/Cylinder.cpp*/
