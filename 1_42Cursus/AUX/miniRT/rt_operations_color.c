/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   color.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/12 19:57:17 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/28 07:26:14 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

unsigned int	color_from_rgb(unsigned char r, unsigned char g,
		unsigned char b)
{
	return ((unsigned int)r | (unsigned int)g << 8 | (unsigned int)b << 16);
}

void			color_get_rgb(unsigned int rgb, unsigned char *r,
		unsigned char *g, unsigned char *b)
{
	*r = rgb & 0xff;
	*g = (rgb >> 8) & 0xff;
	*b = (rgb >> 16) & 0xff;
}
