/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   keyboard.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/15 11:46:21 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/28 07:22:56 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

int		valid_key(int key)
{
	return (key == 53 || key == 123 || key == 124 || key == 0 || key == 2);
}

void	keyboard(int key, t_parse parse)
{
	if (valid_key(key))
	{
		key == 53 ? exit(EXIT_FAILURE) : 0;
		key == 123 || key == 0 ? switch_camera(parse, "left") : 0;
		key == 124 || key == 2 ? switch_camera(parse, "right") : 0;
	}
}
