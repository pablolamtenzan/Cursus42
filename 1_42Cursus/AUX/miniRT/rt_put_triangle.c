/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   triangle.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/14 05:49:48 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/01 00:55:58 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

/*https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/moller-trumbore-ray-triangle-intersection*/

#include "miniRT.h"

/*char		moller_trumbore(t_tri tri, t_ray ray, double *t)
{
	t_vector v0v1 = vec_sub(tri.v1 - tri.v0);
	t_vector v0v2 = vec_sub(tri.v2 - tri.v0);
	t_vector pvec = cross_product(ray.dir, v0v2);
	float det = dot(v0v1, pvec);
	if (det < 0.00000001)
		return (0);
	if (ABS(det) < 0.00000001)
		return (0);
	float invDet = 1/det;
	t_vector tvec = vec_sub(ray.pos, tri.v0);
	double u = dot(tvec, pvec) * invDet;
	if (u < 0|| u > 1)
		return (0);
	t_vector qvec = cross_product(qvec, v0v1);
	double v = dot(ray.dir, qvec) * invDet;
	if (v < 0 || u + v > 1)
		return (0);
	t = dot(v0v2, qvec) *invDet;
	return (1);
}
*/ /* NO 42 format in coments */
char		moller_trumbore(t_parse parse, t_tri tri, t_ray ray, double *t)
{
	parse.eng.v0v1 = vec_sub(tri.v1, tri.v0);
	parse.eng.v0v2 = vec_sub(tri.v2, tri.v0);
	parse.eng.pvec = cross_product(ray.dir, v0v2);
	parse.eng.det = dot(v0v1, pvec);
	if (parse.eng.det < 0.00000001 || ABS(parse.eng.det) < 0.00000001)
		return (0);
	parse.eng.tvec = vec_sub(ray.pos, tri.v0);
	parse.eng.u = dot(parse.eng.tvec, parse.eng.tvec) * 1 / parse.eng.det;
	if (parse.eng.u < 0 || parse.eng.u > 1)
		return (0);
	parse.eng.qvec = cross_product(parse.eng.tvec, parse.eng.v0v1);
	parse.eng.v = dot(ray.dir, parse.eng.qvec) * 1 / parse.eng.det;
	if (parse.eng.v < 0 || parse.eng.u + parse.eng.v > 1)
		return (0);
	*t = dot(parse.eng.v0v2, parse.eng.qvec) * 1 / parse.eng.det;
	return (1);
}
