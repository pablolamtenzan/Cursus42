/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   square.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/14 06:56:08 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/11/28 07:23:34 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

/* https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-plane-and-ray-disk-intersection */

char		plane_intersections(t_parse parse, t_ray ray, double *t)
{
	double denom;
	t_vector normal;
	t_vector l;
	t_vector p0l0;
	
	normal = get_normal(parse.flat.pos, parse.flat.dir);
	l = vec_sub(ray.dir, ray,pos);
	denom = dot(n, l);
	if (denom > 0.0000001)
	{
		p0l0 = vec_sub(parse.flat.pos, ray.pos);
		*t = dot(p0l0, normal) / denom;
		return (t >= 0);
	}
	return (0);
}

/* https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-plane-and-ray-disk-intersection */

char		square_intersections(t_parse parse, t_ray ray, double *t)
{
	if (plane_intersections(parse, ray, &t))
	{
		/* square conditions */
	}
	t = /*distance */
}
