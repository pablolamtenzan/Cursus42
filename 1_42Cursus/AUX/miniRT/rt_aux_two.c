/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   rt_aux_two.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/28 11:34:14 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/08 11:24:06 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

int	ft_strncmp(const char *s1, const char *s2, t_size n)
{
	t_size i;

	i = 0;
	if (n == 0)
		return (0);
	while (i < n - 1 && s1[i] == s2[i] && s1[i] && s2[i])
		i++;
	return ((unsigned char)s1[i] - (unsigned char)s2[i]);
}

