/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   rt_init_resolution.c                             .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/28 07:09:28 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/03 20:19:54 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"
#include "get_next_line.h"

int			search_init_values(t_parse *parse, int fd) /*check line per line the "R" id to pre-define thw window size*/
{
	char *line;
	char *start;

	while (get_next_line(fd, &line) > 0)
	{
		start = line;
		while (*line++)
		{
			if (*line == 'R' && *(line + 1) == ' ')
			{
				if (!(parse_line(line, parse, start)))
				{
					free(start);
					return (ft_error("Error:\nResolution not founded\n"));
				}
				else
				{
					parse->rt &= ~RT_D_RES;
					return (TRUE);
				}
			}
		}
		free(start);
	}
	return (FALSE);
}
