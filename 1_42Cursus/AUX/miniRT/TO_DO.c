/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   TO_DO.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/28 11:45:49 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/03 23:07:27 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

void		put_camera(t_parse *parse, t_env *env)
{
	printf("put_camera\n");
}

/*void		put_cylinder(t_parse *parse, t_env *env)
{
	printf("put_cylinder\n");
}*/

void		put_flat(t_parse *parse, t_env *env)
{
	printf("put_flat\n");
}

void		put_light(t_parse *parse, t_env *env)
{
	printf("put_light\n");
}

void		put_square(t_parse *parse, t_env *env)
{
	printf("put_square\n");
}

void		put_triangle(t_parse *parse, t_env *env)
{
	printf("put_triangle\n");
}

void		set_ambiental_light(t_parse *parse, t_env *env)
{
	printf("set_ambiental_light\n");
}

void		set_resolution(int x, int y, t_env *env)
{
	printf("set_resolution\n");
}

void		ft_save(t_parse *parse, t_env *env)
{
	return ;
}

void		manage_hook(t_env *env)
{
	return ;
}
