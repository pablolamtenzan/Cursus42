/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   rt_aux.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/28 06:43:11 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/08 11:23:15 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

int			ft_error(char *error)
{
	perror(error);
	exit(EXIT_FAILURE);
	return (0);
}

double		d_elem(char *line, int it)
{
	char **ret;

	ret = split_multicharset(line, " ,");
	return ((double)ft_atod(ret[it]));
}

int			i_elem(char *line, int it)
{
	char **ret;

	ret = split_multicharset(line, " ,");
	return ((int)ft_atoi(ret[it]));
}

int		ft_isdigit(int c)
{
	if (c <= '9' && c >= '0')
		return (1);
	else
		return (0);
}

/*int			ft_strlen(const char *str)
{
	int i;

	i = 0;
	while (str[i++])
		;
	return (i);
}*/
