/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   color.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/12 19:57:17 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/09 19:45:42 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

t_color					color(unsigned char r, unsigned char g, unsigned char b)
{
	t_color color;

	color.chanel.r = r;
	color.chanel.g = g;
	color.chanel.b = b;
	return (color);
}

unsigned int			color_from_rgb(unsigned char r, unsigned char g,
		unsigned char b)
{
	return ((unsigned int)r | (unsigned int)g << 8 | (unsigned int)b << 16);
}

void					color_get_rgb(unsigned int rgb, unsigned int *r,
		unsigned int *g, unsigned int *b)
{
	*r = rgb & 0xff;
	*g = (rgb >> 8) & 0xff;
	*b = (rgb >> 16) & 0xff;
}

unsigned int			color_add(unsigned int col_1, unsigned int col_2)
{
	unsigned int	aux;
	t_color			c1;
	t_color			c2;

	c1.val = col_1;
	c2.val = col_2;
	aux = c1.chanel.r + c2.chanel.r;
	c1.chanel.r = aux <= 0xFF ? aux : 0xFF;
	aux = c1.chanel.g + c2.chanel.g;
	c1.chanel.g = aux <= 0xFF ? aux : 0xFF;
	aux = c1.chanel.b + c2.chanel.b;
	c1.chanel.b = aux <= 0xFF ? aux : 0xFF;
	return (c1.val);
}

unsigned int			color_scale(unsigned int color, double n)
{
	t_color		c;

	n = n > 0xFF ? 0xFF : n;
	c.val = color;
	c.chanel.r = ((c.chanel.r * n) <= 0xFF) ? (unsigned)(c.chanel.r * n) : 0xFF;
	c.chanel.g = ((c.chanel.g * n) <= 0xFF) ? (unsigned)(c.chanel.g * n) : 0xFF;
	c.chanel.b = ((c.chanel.b * n) <= 0xFF) ? (unsigned)(c.chanel.b * n) : 0xFF;
	return (c.val);
}
