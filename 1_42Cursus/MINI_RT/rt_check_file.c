/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   rt_check_file.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/11/28 06:50:44 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/08 20:41:52 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"
#include <fcntl.h>

static int			check_file_name(char *fn)
{
	int i;

	i = 0;
	if (*fn == ' ')
		return (FALSE);
	while (*++fn)
		*fn == '.' ? i++ : 0;
	if (i != 1)
		return (FALSE);
	while (42)
	{
		if (*(fn - 1) == 't')
			if (*(fn - 2) == 'r')
				if (*(fn - 3) == '.')
					return (TRUE);
		return (FALSE);
	}
}

int					check_file(int arc, char **argv, int *fd, t_scene *scene)
{
	if (arc >= 2 && check_file_name(argv[1]))
	{
		if (arc >= 2 && arc <= 3)
		{
			*fd = open(argv[1], O_RDONLY);
			if (arc == 3 && ft_strncmp(argv[2], "--save", 6))
				scene->f |= RT_SAVE;
		}
		return (TRUE);
	}
	else
	{
		ft_error("Error:\nNot valid file name\n");
		return (FALSE);
	}
}
