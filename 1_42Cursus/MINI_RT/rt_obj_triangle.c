/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   rt_obj_triangle.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/12/08 18:17:29 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/08 18:31:05 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"
#include <stdlib.h>

t_xy			rt_triangle_collision(t_ray ray, void *obj)
{
	t_xy a;
	return (a);
}

t_vector		rt_triangle_normal(t_vector vec, void *obj)
{
	t_vector a;
	return (a);
}

t_tri			*rt_new_triangle(t_vector v0, t_vector v1, t_vector v2,
		t_color color)
{
	t_tri		*tri;

	if (!(ft_calloc(sizeof(t_tri), 1)))
		return (NULL);
	tri->v0 = v0, 
	tri->v1 = v1; 
	tri->v2 = v2;
	tri->color = color;
	return (tri);
}

void			rt_triangle_obj(t_obj *obj, t_tri *t)
{
	obj->id = e_triangle;
	obj->collision = rt_triangle_collision;
	obj->get_normal = rt_triangle_normal;
	obj->objs = t;
	obj->color = t->color;
}
