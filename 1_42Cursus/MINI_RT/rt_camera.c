/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_camera.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/06 06:19:12 by plamtenz          #+#    #+#             */
/*   Updated: 2020/01/14 00:12:59 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "miniRT.h"

/*t_cam			*rt_new_cam(t_vector pos, double fov)
{
	t_cam	*cam;

	if (!(cam = ft_calloc(sizeof(t_cam), 1)))
		return (NULL);
	cam->pos = pos;
	cam->dir.x = 0;
	cam->dir.y = 0;
	cam->dir.z = 1;
	cam->dir = normalise(cam->dir);
	cam->fov = tan(((fov / 2) * M_PI) / 180);
	cam->view_p_h = 1;
	cam->view_p_w = 1;
	cam->min_t = 0;
	cam->max_t = INFINITY;
	return (cam);
}*/


/*t_vector		rt_cam_to_vp(t_pixel p, t_image *image,
		t_cam *cam)
{
	t_vector	view_dir;
	double		scene;

	scene = (double)image->w / (double)image->h;
	view_dir.x = p.x * ((double)cam->view_p_w / (double)image->w) * scene * cam->fov;
	view_dir.y = p.y * ((double)cam->view_p_h / (double)image->h) * cam->fov;
	view_dir.z = cam->dir.z;
	view_dir = vec_rotation(view_dir, get_rad(cam->ang));
	return (normalise(view_dir));
}*/

/*t_vector		rt_cam_to_vp(t_pixel p, t_image *image,
		t_cam *cam)
{
	t_vector	view_dir;
	double		scene;

	scene = (double)image->w / (double)image->h;
	view_dir.x = (2.0 * (((double)p.x + 0.5) / (double)image->w - 1.0)
		* cam->fov) * scene;
	view_dir.y = (1.0 - 2.0 * (((double)p.y + 0.5) / (double)image->h)
		* cam->fov);
	view_dir.z = -1;
	view_dir = vec_rotation(view_dir, get_rad(cam->ang));
	view_dir = vec_mult_dir_matrix(view_dir);
	return (normalise(view_dir));
}*/

/*t_vector		rt_cam_to_vp(t_pixel p, t_image *image, 
	t_cam *cam)
{
	t_vector up;
	t_vector right;
	t_vector forward;

	forward = cam->dir;

	up.x = 0;
	up.y = 1;
	up.z = 0;

	right = cross_product(forward, up);
	up = cross_product(right, forward);

	// printf("%lf %lf %lf\n", forward.x, forward.y, forward.z);

	t_vector dir;

	const float viewplaneDist = 1.0f;
	const float viewplaneWidth = 0.35f;
	const float viewplaneHeight = 0.5f;

	// vec_scale(cam->dir, viewplaneDist);

	// vec_scale(right, viewplaneWidth / 2.0f);
	//viewPlaneUpLeft = camPos + ((vecDir*viewplaneDist)+(upVec*(viewplaneHeight/2.0f))) - (rightVec*(viewplaneWidth/2.0f))
	t_vector topleft;

	topleft.x =  cam->pos.x + ((cam->dir.x * viewplaneDist) + (up.x * (viewplaneHeight / 2.0f))) - (right.x * (viewplaneWidth / 2.0f));
	topleft.y =  cam->pos.y + ((cam->dir.y * viewplaneDist) + (up.y * (viewplaneHeight / 2.0f))) - (right.y * (viewplaneWidth / 2.0f));
	topleft.z =  cam->pos.z + ((cam->dir.z * viewplaneDist) + (up.z * (viewplaneHeight / 2.0f))) - (right.z * (viewplaneWidth / 2.0f));

	float xIndent = viewplaneWidth / (float)image->w;
	float yIndent = viewplaneHeight / (float)image->h; 

//viewPlaneUpLeft + rightVec*xIndent*x - upVec*yIndent*y
	dir.x = topleft.x + right.x * xIndent * p.x - up.x * yIndent * p.y;
	dir.y = topleft.y + right.y * xIndent * p.x - up.y * yIndent * p.y;
	dir.z = topleft.z + right.z * xIndent * p.x - up.z * yIndent * p.y;

	printf("%g %g %g\n", dir.x, dir.y, dir.z);
	return (dir);
}*/


t_vector		rt_cam_to_vp(t_pixel p, t_image *image, 
	t_cam *cam)
	{
		t_vector	pos_vp_up_left;
		t_vector	vp_center_up;
		t_vector	vp_up_right;
		double		vp_width;
		double		vp_height;

		static float 	b = 1;

		//printf ("%f\n", b);

		vp_width = cam->fov * b * (image->w / image->h);
		vp_height = cam->fov * b;
		vp_width = cam->fov * b * 5 * (image->w / image->h);
		vp_height = cam->fov * b * 5;		

		vp_center_up = vec_add(vec_scale(cam->dir, b), vec_scale(vector(0,1,0),
			vp_height / 2.0));
		vp_up_right = vec_scale(vector(1,0,0), vp_width / 2.0);
		pos_vp_up_left = vec_add(cam->pos, vp_center_up);
		pos_vp_up_left = vec_sub(pos_vp_up_left, vp_up_right);
		pos_vp_up_left = vec_add(pos_vp_up_left, vec_scale(vec_scale(vector(1,0,0), vp_width / image->w), (double)p.x));
		pos_vp_up_left = vec_sub(pos_vp_up_left, vec_scale(vec_scale(vector(0,1,0), vp_height / image->h), (double)p.y));
		return (normalise(vec_sub(pos_vp_up_left, cam->pos)));
		// have to normatise and test normalise;
	}





























/* https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-generating-camera-rays/generating-camera-rays */











void			rt_write_help(t_mlx *w, t_cam *c)
{
	int start;
	int i;
	char *ret;
	
	asprintf(&ret, "%lf", c->dir.x);
	mlx_string_put(w->mlx_ptr, w->win_ptr, w->max_w - 300 , w->max_h - 20 , 0x00FF0000 , ret);
		asprintf(&ret, "%lf", c->dir.y);
	mlx_string_put(w->mlx_ptr, w->win_ptr, w->max_w - 210 , w->max_h - 20 , 0x00FF0000 , ret);
		asprintf(&ret, "%lf", c->dir.z);
	mlx_string_put(w->mlx_ptr, w->win_ptr, w->max_w - 130 , w->max_h - 20 , 0x00FF0000 , ret);

}
