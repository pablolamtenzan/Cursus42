/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_image.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/06 02:44:40 by plamtenz          #+#    #+#             */
/*   Updated: 2020/01/14 03:18:04 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "miniRT.h"
#include <stdlib.h>

void					rt_pixel_to_image(t_image *image, t_pixel *p)
{
	/*image->str[((image->w * (image->max_h - p->y)) +
			(p->x + image->max_w))] = p->color.val;*/

			
			*(int *)(image->str + (p->x * 4 + (image->line_size * p->y))) = p->color.val; // correct DO IT
			
	//image->str[p->y * image->w + p->x] = p->color.val;

	//image->str[p->y * image->w + p->x] = p->color.val;
}

t_mlx					*rt_mlx_init(char *window_name, int h, int w)
{
	t_mlx	*win;

	if (!(win = ft_calloc(sizeof(t_mlx), 1)))
		return (NULL);
	win->max_h = h;
	win->max_w = w;
	if (!(win->mlx_ptr = mlx_init()))
		return (NULL);
	if (!(win->win_ptr = mlx_new_window(win->mlx_ptr, win->max_w, win->max_h,
					!window_name ? "plamtenz's miniRT is too clean" :
					window_name)))
		return (NULL);
	return (win); /*return of mlx_init used for image init*/
}

int						rt_remake_image(t_mlx *win)
{
	if (!(win->image.ptr = mlx_new_image(win->mlx_ptr, win->image.w,
					win->image.h)))
		return (FALSE);
	if (!(win->image.str = mlx_get_data_addr(win->image.ptr,
					&(win->image.bitperpixel), &(win->image.line_size),
					&(win->image.endian))))
		return (FALSE);
	return (TRUE);
}

int						rt_image_init(t_mlx *win)
{
	if (win->image.ptr)
		mlx_destroy_image(win->mlx_ptr, win->image.ptr);
	win->image.h = win->max_h;
	win->image.w = win->max_w;
	win->image.bitperpixel = sizeof(int) * 8;
	win->image.line_size = sizeof(int) * win->image.w;
	win->image.endian = 0;
	win->image.max_h = win->max_h / 2;
	win->image.min_h = -(win->max_h / 2);
	win->image.max_w = win->max_w / 2;
	win->image.min_w = -(win->max_w / 2);
	return (rt_remake_image(win)); /* using mlx images */
}

void					rt_destroy_all(t_mlx *win)
{
	if (win->image.ptr)
		mlx_destroy_image(win->mlx_ptr, win->image.ptr);
	mlx_destroy_window(win->mlx_ptr, win->win_ptr);
}
