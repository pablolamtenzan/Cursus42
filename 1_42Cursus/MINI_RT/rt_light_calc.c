/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_light_calc.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/06 01:28:07 by plamtenz          #+#    #+#             */
/*   Updated: 2019/12/16 19:37:54 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "miniRT.h"

static double		rt_get_shine(t_vector view, t_vector light,
		t_vector normal, double sh)
{
	double		res;
	t_vector	reflex;

	reflex = vec_sub(vec_scale(vec_scale(normal, 2.0) , dot(normal, light)),
			light);
	res = dot(reflex, view);
	return (res >= 0 ? pow(res / (vec_len(reflex) * vec_len(view)), sh) : 0.0);
}

static t_xy			rt_get_light(t_ray ray, t_light *light, t_scene *s,
		t_vector normal)
{
	t_rt			rt;
	t_vector		light_dir;
	t_xy			res;
	double			diff;

	res.x = 0;
	res.y = 0;
	ray.dir = normalise(vec_sub(light->pos, ray.pos));
	if (!rt_ray_trace(ray, s, &rt, vec_len(light_dir)))
	{
		res.y = rt_get_shine(vec_sub(s->cam->pos, ray.pos), ray.dir, normal,
				80.0);
		res.x = light->intensity;
		diff = dot(normal, ray.dir);
		res.x *= (diff  >= 0) ? diff : 0;
	}
	return (res);
}

unsigned int		rt_light_calc(t_ray ray, t_scene *s, t_rt rt)
{
	t_rt		res;
	int			nb_light;
	t_vector	normal;
	t_xy		diff_shine;

	if (rt.t.x < 0)
		return (FALSE);
	ray.pos = vec_add(ray.pos, vec_scale(ray.dir, rt.t.x - 0.000000001));
	normal = rt.obj->get_normal(ray.pos, rt.obj->objs);
	nb_light = s->light_num;
	res.t.x = 0.0;
	res.color.val = 0;
	while (nb_light--)/*for ecah light */
	{
		if (s->light->type == L_POINT)
		{
			diff_shine = rt_get_light(ray, s->light, s, normal);
			res.t.x += diff_shine.x;
			res.color.val = color_add(res.color.val, color_scale(color_scale( /*put this shit in a fcntlt to norme this shit*/
							s->light->color.val, diff_shine.y), diff_shine.x));
		}
		else if (s->light->type == L_AMBIENT)
			res.t.x += s->light->intensity; /*get lum amb + intensity if !collision */
		s->light = s->light->next;
	}
	return (color_add(color_scale(rt.obj->color.val, res.t.x), res.color.val)); /*add and scale to obj->color */
}

		/* FOR EACH LIGHT */
/* light_factor + ((light_color * shine) * intensity) + (obj_color * shine) = light_factor (POINT) */
/* light_factor + (obj_color * intensity) = light_factor (AMBIENT) */
