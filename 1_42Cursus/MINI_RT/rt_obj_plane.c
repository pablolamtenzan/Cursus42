/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   rt_plane.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/12/05 02:19:28 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/10 02:10:23 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"
#include <stdlib.h>

t_xy		rt_plane_collision(t_ray r, void *obj)
{
	t_plane		*p;
	t_xy		ab;
	t_xy		res;

	res.x = -1;
	res.y = -1;
	p = (t_plane *)obj;
	if ((ab.y = dot(p->dir, vec_sub(r.dir, p->dir))) >= ZERO)
	{
		ab.x = dot(p->dir, vec_sub(r.pos, p->pos));
		res.x = -ab.x / ab.y;
		res.y = res.x;
	}
	return (res);
}

t_vector	rt_plane_normal(t_vector vec, void *obj)
{
	t_plane		*p;

	p = (t_plane *)obj;
	if (dot(p->dir, vec) > 0)
		return (vec_scale(p->dir, -1));
	return (p->dir);
}

t_plane		*rt_new_plane(t_vector pos, t_vector dir, t_color color)
{
	t_plane		*p;

	if (!(p = ft_calloc(sizeof(t_plane), 1)))
		return (NULL);
	p->pos = pos;
	p->dir = dir;
	p->color = color;
	return (p);
}

void		rt_plane_obj(t_obj *obj, t_plane *p)
{
	obj->id = e_plane;
	obj->collision = rt_plane_collision;
	obj->get_normal = rt_plane_normal;
	obj->objs = p;
	obj->color = p->color;
}
