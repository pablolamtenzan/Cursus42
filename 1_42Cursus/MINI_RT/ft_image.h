/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_image.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/06 00:41:07 by plamtenz          #+#    #+#             */
/*   Updated: 2020/01/14 03:16:34 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef FT_IMAGE_H
# define FT_IMAGE_H

#include "ft_color.h"
#include "mlx.h"
//#include "mlx_int.h"

typedef enum				e_keys
{
	e_key_esc = 53,
	e_key_espace = 49,
	e_key_enter = 76,
	e_key_up = 126,
	e_key_down = 125,
	e_key_right = 124,
	e_key_left = 123,
	e_key_a = 0,
	e_key_s = 1,
	e_key_d = 2,
	e_key_w = 13,
}							t_keys;

# define KEY_ESC			53
# define KEY_ESP			49
# define KEY_ENTER			76
# define KEY_UP				126
# define KEY_DOWN			125
# define KEY_RIGHT			124
# define KEY_LEFT			123
# define KEY_A				0
# define KEY_S				1
# define KEY_D				2
# define KEY_W				13

typedef struct				s_image
{
	void					*ptr;
	char			*str;
	int						bitperpixel;
	int						line_size;
	int						endian;
	int						h;
	int						w;
	int						max_h;
	int						min_h;
	int						max_w;
	int						min_w;
}							t_image;

typedef struct				s_mlx
{
	void					*mlx_ptr;
	void					*win_ptr;
	int						max_h;
	int						max_w;
	t_image					image;
}							t_mlx;

typedef struct				s_pixel
{
	int						x;
	int						y;
	t_color					color;
}							t_pixel;

typedef struct				s_bmp
{
	unsigned short			bf_word;
	unsigned int			bf_size;
	unsigned short			bf_reserved1;
	unsigned short			bf_reserved2;
	unsigned int			bf_offset;
	unsigned int			bi_size;
	unsigned int			bi_width;
	unsigned int			bi_height;
	unsigned short			bi_planes;
	unsigned short			bi_bitcount;
	unsigned int			bi_compresion;
	unsigned int			bi_imgsize;
	unsigned int			bi_xpelpermeter;
	unsigned int			bi_ypelpermeter;
	unsigned int			bi_clr_used;
	unsigned int			bi_clr_imp;
}							t_bmp;

/*
**-----------------------------DISPLAY-----------------------------------------
*/
void						rt_pixel_to_image(t_image *image, t_pixel *p);
t_mlx						*rt_mlx_init(char *window_name, int h, int w);
int							rt_image_init(t_mlx *win);
int							rt_remake_image(t_mlx *win);


/*
**--------------------------ERROR AND DESTROY----------------------------------
*/
void						rt_destroy_all(t_mlx *win);
void						rt_error(char *error_mesage, t_mlx *win);
void						rt_exit(t_mlx *win);

#endif
