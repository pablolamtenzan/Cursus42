/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 23:01:32 by plamtenz          #+#    #+#             */
/*   Updated: 2019/12/14 18:43:58 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef FT_VECTOR_H
# define FT_VECTOR_H

# include <math.h>

typedef struct				s_xy
{
	double					x;
	double					y;
}							t_xy;

typedef struct				s_vector
{
	double						x;
	double						y;
	double						z;
}							t_vector;

typedef struct				s_ray
{
	t_vector				pos;
	t_vector				dir;
}							t_ray;

/*
**--------------------------CREATE VECTOR ELEMS--------------------------------
*/
t_vector					vector(double x, double y, double z);
t_ray						set_ray(t_vector pos, t_vector dir);

/*
**-----------------------------TRIG OPERATIONS---------------------------------
*/
double						quad_equ_d(double a, double b, double c);
int							v_quad_equ(double a, double b, double c, t_xy *res);

/*
**----------------------------VECTOR OPERATIONS--------------------------------
*/
t_vector					vec_sub(t_vector v1, t_vector v2);
t_vector					vec_add(t_vector v1, t_vector v2);
t_vector					vec_scale(t_vector v, double c);
t_vector					vec_div(t_vector v, double c);
double						vec_len(t_vector a);
double						dot(t_vector v1, t_vector v2);
t_vector					normalise(t_vector v);
t_vector					cross_product(t_vector v1, t_vector v2);

/*
**----------------------VECTOR ROTATIONS AND TRANSLATION-----------------------
*/
t_vector					vec_rotation(t_vector dir, t_vector rad);
t_vector					get_rad(t_vector ang);
t_vector					vec_mult_dir_matrix(t_vector v);

#endif
