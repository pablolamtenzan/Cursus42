/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_operations2.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 23:11:53 by plamtenz          #+#    #+#             */
/*   Updated: 2020/01/14 02:03:12 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "ft_vector.h"

double			quad_equ_d(double a, double b, double c)
{
	return ((b * b) - (4 * a * c));
}

int				v_quad_equ(double a, double b, double c, t_xy *res)
{
	double	d;
	t_xy	re;

	d = quad_equ_d(a, b, c);
	if (d < 0 || a < 0)
		return (0);
	re.x = (-b + sqrt(d)) / (2.0 * a);
	re.y = (-b - sqrt(d)) / (2.0 * a);
	if (res)
		*res = re;
	return (1);
}

t_vector		vec_rotation(t_vector dir, t_vector rad)
{
	t_vector	res;

	res.x = (((dir.x * cos(rad.y)) - (dir.z * cos(rad.x)) -
				(dir.y * sin(rad.x)) * sin(rad.y) * cos(rad.z)) +
			(((dir.y * cos(rad.x)) + (dir.z * sin(rad.x))) * sin(rad.z)));
	res.y = (((dir.y * cos(rad.x)) - (dir.z * sin(rad.x))) * cos(rad.z)) -
				(((dir.x * cos(rad.y)) - (((dir.z * cos(rad.x)) -
			(dir.y * sin(rad.x))) * sin(rad.x))) * sin(rad.z));
	res.z = (((dir.z * cos(rad.x)) - (dir.y * sin(rad.x))) *
			cos(rad.y)) + (dir.x * sin(rad.y));
	return (res);
}

t_vector		get_rad(t_vector ang)
{
	t_vector	rad;

	rad.x = M_PI * ang.x / 180;
	rad.y = M_PI * ang.y / 180;
	rad.z = M_PI * ang.z / 180;
	return (rad);
}

t_vector		vec_mult_dir_matrix(t_vector vec)
{
	t_vector	v;
	double		a;
	double		b;
	double		c;

	a = vec.x * 1.0 + vec.y + 0.0 + vec.z + 0.0 * 0.0;
	b = vec.x * 0.0 + vec.y + 1.0 + vec.z + 0.0 * 1.0;
	c = vec.x + 0.0 + vec.y + 0.0 + vec.z + 1.0 * 0.0;
	v.x = a;
	v.y = b;
	v.z = c;
	return (v);
}