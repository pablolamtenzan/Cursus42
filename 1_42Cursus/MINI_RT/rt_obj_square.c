/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   rt_obj_square.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/12/08 17:56:34 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/08 20:25:46 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"
#include <stdlib.h>

t_xy				rt_square_collision(t_ray r, void *obj)
{
	t_xy a;
	return (a);
}

t_vector			rt_square_normal(t_vector vec, void *obj)
{
	t_square		*sq;

	sq = (t_square *)obj;
	if (dot(sq->dir , vec) > 0)
		return (vec_scale(sq->dir, -1));
	return (sq->dir);
}

t_square			*rt_new_square(t_vector pos, t_vector dir, double height,
		t_color color)
{
	t_square		*sq;

	if (!(ft_calloc(sizeof(t_square), 1)))
		return (NULL);
	sq->pos = pos;
	sq->dir = dir;
	sq->height = height;
	sq->color = color;
	return (sq);
}

void				rt_square_obj(t_obj *obj, t_square *sq)
{
	obj->id = e_square;
	obj->collision = rt_square_collision;
	obj->get_normal = rt_square_normal;
	obj->objs = sq;
	obj->color = sq->color;
}
