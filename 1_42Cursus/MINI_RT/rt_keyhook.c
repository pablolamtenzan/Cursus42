/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_keyhook.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/06 07:54:33 by plamtenz          #+#    #+#             */
/*   Updated: 2019/12/15 18:27:58 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "miniRT.h"

static void			rt_swith_cam(int keycode, t_scene *scene)
{
	if (keycode == KEY_A || keycode == KEY_S || keycode == KEY_D
			|| keycode == KEY_W)
	{
		if (keycode == KEY_A || keycode == KEY_S)
			scene->cam = scene->cam->back;
		else if (keycode == KEY_W || keycode == KEY_D)
			scene->cam = scene->cam->next;
	}
}

static void			rt_rotate_cam(int keycode, t_scene *scene)
{
	if (keycode == KEY_UP || keycode == KEY_DOWN || keycode == KEY_DOWN
			|| keycode == KEY_LEFT)
	{
		if (keycode == KEY_UP)
			scene->cam->ang.x += 15;
		else if (keycode == KEY_DOWN)
			scene->cam->ang.x -= 15;
		else if (keycode == KEY_RIGHT)
			scene->cam->ang.y += 15;
		else if (keycode == KEY_LEFT)
			scene->cam->ang.y -= 15;
		scene->cam->rad = get_rad(scene->cam->ang);
	}
}

static void 		rt_exit_key(int keycode, t_mlx *win)
{
	if (keycode == KEY_ESC) /* have to add click on red cross*/
		rt_exit(win);
}

static int			rt_keys(int keycode, t_scene *scene)
{
	if (scene->cam->next && scene->cam->back)
		rt_swith_cam(keycode, scene);
	rt_rotate_cam(keycode, scene);
	rt_exit_key(keycode, scene->win);
	rt_draw(scene);
	return (1);
}


void				rt_keyhook(t_scene *scene)
{
	mlx_hook(scene->win->win_ptr, 2, 0, &rt_keys, scene);
	//mlx_hook(scene->win->win_ptr, 17, , rt_exit_hook, scene->win);
	mlx_loop(scene->win->mlx_ptr);
}
