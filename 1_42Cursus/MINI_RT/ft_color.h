/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/08 07:54:37 by plamtenz          #+#    #+#             */
/*   Updated: 2020/01/14 00:48:22 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef FT_COLOR_H
# define FT_COLOR_H

typedef struct				s_color_chanels
{
	unsigned char			b;
	unsigned char			g;
	unsigned char			r;
}							t_color_chanels;

typedef union				u_color
{
	t_color_chanels			chanel;
	unsigned int			val : 24;
}							t_color;

/*
**--------------------------CREATE NEW COLOR-----------------------------------
*/
t_color						color(unsigned char r, unsigned char g,
		unsigned char b);

/*
**--------------------------COLOR OPERATIONS-----------------------------------
*/
unsigned int				color_add(unsigned int col_1, unsigned int col_2);
unsigned int				color_scale(unsigned int c, double n);

/*
**------------------------COLOR TRANSFORMATIONS--------------------------------
*/
unsigned int				color_from_rgb(unsigned char r, unsigned char g,
		unsigned char b);
void						color_get_rgb(unsigned int rgb, unsigned int *r,
		unsigned int *g, unsigned int *b);

#endif
