/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   miniRT.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/04 21:45:00 by plamtenz          #+#    #+#             */
/*   Updated: 2019/12/15 21:26:26 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef MINIRT_H
# define MINIRT_H

# include "ft_objects.h"
# include "ft_image.h"

# define TRUE				1
# define FALSE				0

# define RT_RES				(1 << 0)
# define RT_AMB				(1 << 1)
# define RT_SAVE			(1 << 2)

typedef unsigned long		t_size;

typedef enum			e_light_type
{
	e_ambient,
	e_point_light,
}						t_light_type;

# define L_AMBIENT		0
# define L_POINT		1

typedef struct			s_cam
{
	int					num;
	void				*start;
	void				*next;
	void				*back;
	t_vector			pos;
	t_vector			dir;
	t_vector			ang;
	t_vector			rad;
	double				fov;
	double				view_p_h;
	double				view_p_w;
	double				max_t;
	double				min_t;
}						t_cam;

typedef struct			s_light
{
	void				*next;
	void				*start;
	t_vector			pos;
	double				intensity;
	double				fadding;
	int					type;
	t_color				color;
}						t_light;

typedef struct			s_rt
{
	t_xy				t;
	t_obj				*obj;
	t_color				color;
}						t_rt;

typedef struct			s_scene
{
	int					f;
	short				objs_num;
	t_obj				*obj;
	short				light_num;
	t_light				*light;
	t_cam				*cam;
	t_mlx				*win;
}						t_scene;

/*
**--------------------------MAIN FUNCTIONS-------------------------------------
*/
void						minirt(int arc, char **argv);
void						rt_parse(int fd, t_scene *scene);
void						rt_draw(t_scene *scene);
void						rt_draw_image(t_mlx *mlx, t_cam *c);
void						rt_write_help(t_mlx *w, t_cam *c);
/*
**----------------------------RAY TRACING--------------------------------------
*/
unsigned int				rt_light_calc(t_ray ray, t_scene *s, t_rt rt);
int							rt_ray_trace(t_ray r, t_scene *s, t_rt *rt,
							double lim_t);
void						rt_engine(t_mlx *mlx, t_cam *c, t_scene *s);
/*
**----------------------------CAM FUNCTIONS------------------------------------
*/
t_cam						*rt_new_cam(int num);
t_vector					rt_cam_to_vp(t_pixel p,
							t_image *image, t_cam *cam);

/*
**---------------------------MANAGE KEYS---------------------------------------
*/
void						rt_keyhook(t_scene *scene);

/*
**-------------------------LINKED LIST ALLOCATION------------------------------
*/
t_obj						*rt_new_obj(int id);
t_light						*rt_new_light(int type);
t_scene						*rt_new_scene(void);

/*
**--------------------------ERROR AND EXIT-------------------------------------
*/
void						rt_exit(t_mlx *win);
void						ft_error(char *error_msg);

/*
**---------------------OPEN , SAVE AND CHECK FILE ERROR------------------------
*/
int							check_file(int arc, char **argv, int *fd,
		t_scene *scene);

/*
**----------------------------PARSE BASICS-------------------------------------
*/
int							rt_parse_line(t_scene *scene, char *line);
int							rt_parse_light(t_scene *scene, char *line);
int							rt_parse_obj(t_scene *scene, char *line);
int							rt_parse_cam(t_scene *scene, char *line);

/*
**-------------------------PARSE ERROR CHECK-----------------------------------
*/
int							rt_parse_error_res(t_scene *scene, char *line);
int							rt_parse_error_amb(t_light *light, char *line);
int							rt_parse_error_point(t_light *light, char *line);
int							rt_parse_error_cam(t_cam *cam, char *line);
int							rt_parse_error_plane(t_obj *obj, char *line);
int							rt_parse_error_sphere(t_obj *obj, char *line);
int							rt_parse_error_cylinder(t_obj *obj, char *line);
int							rt_parse_error_square(t_obj *obj, char *line);
int							rt_parse_error_triangle(t_obj *obj, char *line);

/*
**---------------------------AUX FUNCTIONS-------------------------------------
*/
//void						ft_bzero(void *str, t_size n);
void						*ft_calloc(t_size nitems, t_size n);
int							ft_strncmp(const char *s1, const char *s2,
		t_size n);
void						ft_objadd_back(t_obj **alst, t_obj *nw);
void						ft_lightadd_back(t_light **alst, t_light *nw);
void						ft_camadd_back(t_cam **alst, t_cam *nw);
int							ft_atoi(const char *str);
double						ft_atod(char *str);
char						**split_multicharset(char *str, char *charset);
int							ft_isdigit(int c);
char						*ft_itoa(int n);
char						*ft_dtoa(double n, int precision);

#endif







/*possible srcs:
 *
 * https://github.com/AzazelloKAY/ft_rtv1/tree/master/sources
 *
 * https://github.com/vnguyen42/rtv1/blob/master/ft_rtv1.h */

