/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_parse_error2.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/07 07:18:15 by plamtenz          #+#    #+#             */
/*   Updated: 2019/12/14 18:45:08 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "miniRT.h"

int				rt_parse_error_plane(t_obj *obj, char *line)
{
	t_plane			p;
	char			**values;

	values = split_multicharset(line, " ,");
	if (!values[9] ||values[10])
		return (FALSE);
	p.pos = vector(ft_atod(values[1]), ft_atod(values[2]), ft_atod(values[3]));
	if ((p.dir.x = ft_atod(values[4])) < -1 || p.dir.x > 1)
		return (FALSE);
	if ((p.dir.y = ft_atod(values[5])) < -1 || p.dir.y > 1)
		return (FALSE);
	if ((p.dir.z = ft_atod(values[6])) < -1 || p.dir.z > 1)
		return (FALSE);
	if ((p.color.chanel.r = ft_atoi(values[7])) < 0 || p.color.chanel.r > 0xFF)
		return (FALSE);
	if ((p.color.chanel.g = ft_atoi(values[8])) < 0 || p.color.chanel.g > 0xFF)
		return (FALSE);
	if ((p.color.chanel.b = ft_atoi(values[9])) < 0 || p.color.chanel.b > 0xFF)
		return (FALSE);
	rt_plane_obj(obj, rt_new_plane(p.pos, p.dir, p.color));
	return (TRUE);
}

int				rt_parse_error_sphere(t_obj *obj, char *line)
{
	t_sphere		sp;
	char			**values;

	values = split_multicharset(line, " ,");
	if (!values[7] || values[8])
		return (FALSE);
	sp.pos = vector(ft_atod(values[1]), ft_atod(values[2]), ft_atod(values[3]));
	if ((sp.d = ft_atod(values[4])) < 0)
		return (FALSE);
	if ((sp.color.chanel.r = ft_atoi(values[5])) < 0
			|| sp.color.chanel.r > 0xFF)
		return (FALSE);
	if ((sp.color.chanel.g = ft_atoi(values[6])) < 0
			|| sp.color.chanel.g > 0xFF)
		return (FALSE);
	if ((sp.color.chanel.b = ft_atoi(values[7])) < 0
			|| sp.color.chanel.b > 0xFF)
		return (FALSE);
	rt_sphere_obj(obj, rt_new_sphere(sp.pos, sp.d, sp.color));
	return (TRUE);
}
static int		rt_parse_cylinder_help(t_obj *obj, t_cyl cy, char **values)
{
	if ((cy.color.chanel.r = ft_atoi(values[7])) < 0
			|| cy.color.chanel.r > 0xFF)
		return (FALSE);
	if ((cy.color.chanel.g = ft_atoi(values[8])) < 0
			|| cy.color.chanel.g > 0xFF)
		return (FALSE);
	if ((cy.color.chanel.b = ft_atoi(values[9])) < 0
			|| cy.color.chanel.b > 0xFF)
		return (FALSE);
	rt_cyl_obj(obj, rt_new_cyl(set_ray(cy.pos, cy.dir), cy.height, cy.d,
				cy.color));
	return (TRUE);
}

int				rt_parse_error_cylinder(t_obj *obj, char *line)
{
	t_cyl			cy;
	char			**values;

	values = split_multicharset(line, " ,");
	if (!values[11] || values[12])
		return (FALSE);
	cy.pos = vector(ft_atod(values[1]), ft_atod(values[2]), ft_atod(values[3]));
	if ((cy.dir.x = ft_atod(values[4])) < -1 || cy.dir.x > 1)
		return (FALSE);
	if ((cy.dir.y = ft_atod(values[5])) < -1 || cy.dir.y > 1)
		return (FALSE);
	if ((cy.dir.z = ft_atod(values[6])) < -1 || cy.dir.z > 1)
		return (FALSE);
	if ((cy.d = ft_atod(values[10])) < 0)
		return (FALSE);
	if ((cy.height = ft_atod(values[11])) < 0)
		return (FALSE);
	return (rt_parse_cylinder_help(obj, cy, values));
}
