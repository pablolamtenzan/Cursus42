/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   rt_manage_lst.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/12/07 06:02:13 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/09 23:37:30 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

int			rt_parse_light(t_scene *scene, char *line)
{
	t_light			*light;
	int				res;

	if (!ft_strncmp(line, "A ", 2) || !ft_strncmp(line, "l ", 2))
	{
		if (scene->f & RT_AMB)
			return (FALSE);
		!ft_strncmp(line, "A ", 2) ? scene->f |= RT_AMB : 0;
		light = !ft_strncmp(line, "A ", 2) ? rt_new_light(L_AMBIENT) :
			rt_new_light(L_POINT);
		ft_lightadd_back(&scene->light, light);
		res = !ft_strncmp(line, "A ", 2) ? rt_parse_error_amb(light, line)
			: rt_parse_error_point(light, line);
		if (res > 0)
			return (TRUE);
	}
	return (FALSE);
}

int			rt_parse_obj(t_scene *scene, char *line)
{
	t_obj			*obj;
	int				id;

	id = -1;
	!ft_strncmp(line, "pl ", 3) ? id = 1 : 0;
	!ft_strncmp(line, "sq ", 3) ? id = 2 : 0;
	!ft_strncmp(line, "sp ", 3) ? id = 0 : 0;
	!ft_strncmp(line, "cy ", 3) ? id = 4 : 0;
	!ft_strncmp(line, "tr ", 3) ? id = 5 : 0;
	if (id < 0)
		return (FALSE);
	obj = rt_new_obj(id);
	ft_objadd_back(&scene->obj, obj);
	id == 1 ? id = rt_parse_error_plane(obj, line) : 0;
	id == 2 ? id = rt_parse_error_square(obj, line) : 0;
	id == 0 ? id = rt_parse_error_sphere(obj, line) : 0;
	id == 4 ? id = rt_parse_error_cylinder(obj, line) : 0;
	id == 5 ? id = rt_parse_error_triangle(obj, line) : 0;
	if (id < 1)
		return(FALSE);
	return (TRUE);
}

int			rt_parse_cam(t_scene *scene, char *line)
{
	t_cam			*cam;
	int				res;

	res = scene->cam ? scene->cam->num + 1 : 1;
	cam = rt_new_cam(res);
	ft_camadd_back(&scene->cam, cam);
	if ((rt_parse_error_cam(cam, line)) > 0)
		return (TRUE);
	return (FALSE);
}
