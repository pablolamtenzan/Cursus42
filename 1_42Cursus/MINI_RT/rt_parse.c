/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_parse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/07 02:04:19 by plamtenz          #+#    #+#             */
/*   Updated: 2020/01/14 04:44:47 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "miniRT.h"
#include "get_next_line.h"

int				rt_parse_line(t_scene *scene, char *line)
{
	while (*line == ' ')
		line++;
	if (!ft_strncmp(line, "R ", 2))
		return (rt_parse_error_res(scene, line));
	else if (!ft_strncmp(line, "A ", 2) || !ft_strncmp(line, "l ", 2))
	{
		scene->light_num++;
		return (rt_parse_light(scene, line));
	}
	else if (!ft_strncmp(line, "pl ", 3) || !ft_strncmp(line, "sq ", 3) ||
			!ft_strncmp(line, "cy ", 3) || !ft_strncmp(line ,"tr ", 3) ||
			!ft_strncmp(line, "sp ", 3))
	{
		scene->objs_num++;
		return (rt_parse_obj(scene, line));
	}
	else if (!ft_strncmp(line, "c ", 2))
		return (rt_parse_cam(scene, line));
	else
		return (FALSE);
}
 
void			rt_parse(int fd, t_scene *scene)
{
	char		*line;
	char		*start;
	int			ret;

	while ((ret = get_next_line(fd, &line)) > 0)
	{
		start = line;
		if (!rt_parse_line(scene, line) && ret != 1) /* ret != 1 fcks my error gestion*/
			ft_error("Error:\n\tNo valid Input\n\n");
		free(start);
		start = NULL;
	}
	if (scene->cam)
	{
		while (scene->cam->next)
			scene->cam = scene->cam->next;
	scene->cam->next = scene->cam->start;
	}
	if (scene->obj)
	{
		while (scene->obj->next)
			scene->obj = scene->obj->next;
	scene->obj->next = scene->obj->start;
	}
	if (scene->light)
	{
		while (scene->light->next)
			scene->light = scene->light->next;
	scene->light->next = scene->light->start;
	}
}
