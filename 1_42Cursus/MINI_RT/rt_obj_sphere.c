/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   rt_sphere.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/12/05 00:28:38 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/10 07:54:59 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"
#include <stdlib.h>

t_xy			rt_sphere_collision(t_ray r, void *obj)
{
	t_sphere	*s;
	t_vector	abc;
	t_vector	len;
	t_xy		qres;

	qres.x = -1;
	qres.y = -1;
	s = (t_sphere *)obj;
	abc.x = dot(r.dir, r.dir);
	len = vec_sub(r.pos, s->pos);
	abc.y = 2.0 * dot(r.dir, len);
	abc.z = dot(len, len) - ((s->d / 2) * (s->d / 2));
	v_quad_equ(abc.x, abc.y, abc.z, &qres);
	return (qres);
}

t_vector		rt_sphere_normal(t_vector vec, void *obj)
{
	t_sphere	*s;
	t_vector	res;

	s = (t_sphere *)obj;
	res = normalise(vec_sub(vec, s->pos));
	return (res);
}

t_sphere		*rt_new_sphere(t_vector pos, double d, t_color color)
{
	t_sphere	*s;

	if (!(s = ft_calloc(sizeof(t_cyl), 1)))
		return (NULL);
	s->color = color;
	s->pos = pos;
	s->d = d;
	return (s);
}

void		rt_sphere_obj(t_obj *obj, t_sphere *s)
{
	obj->id = e_sphere;
	obj->collision = rt_sphere_collision;
	obj->get_normal = rt_sphere_normal;
	obj->objs = s;
	obj->color = s->color;
}
