/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   error.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/12/05 06:21:51 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/08 20:21:17 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"
#include <stdlib.h>
#include <stdio.h>

void			rt_exit(t_mlx *win)
{
	rt_destroy_all(win);
	exit(EXIT_FAILURE);
}

void			ft_error(char *error_msg)
{
	perror(error_msg);
	exit(EXIT_FAILURE);
}
