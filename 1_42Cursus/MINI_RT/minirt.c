/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   minirt.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/12/07 11:05:33 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/10 07:54:57 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"
#include <stdlib.h>

void		rt_draw(t_scene *scene)
{
	rt_engine(scene->win, scene->cam, scene);
	rt_draw_image(scene->win, scene->cam);
}

void		rt_draw_image(t_mlx *win, t_cam *c)
{
	mlx_clear_window(win->mlx_ptr, win->win_ptr);
	mlx_put_image_to_window(win->mlx_ptr, win->win_ptr, win->image.ptr, 0, 0);
	rt_write_help(win, c);
}

void		minirt(int arc, char **argv)
{
	t_scene		*scene;
	int			fd;

	if (!(scene = rt_new_scene()))
		ft_error("");
	check_file(arc, argv, &fd, scene);
	rt_parse(fd, scene);
	if (!(scene->win = rt_mlx_init(NULL, scene->win->max_h, scene->win->max_w)))
		ft_error("");
	if (!(rt_image_init(scene->win)))
		ft_error("");
	rt_draw(scene);
	rt_keyhook(scene);

}
/* 
 *
 * to do : my cam perse is clean by my init is trash ior shit i have to test this better fck m y live */
