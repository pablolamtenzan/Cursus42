/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   rt_objects.h                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/12/04 23:26:36 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/10 01:25:49 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_OBJECTS_H
# define FT_OBJECTS_H

# include "ft_vector.h"
# include "ft_color.h"

# define ZERO				0.00000000000000000000000000000000000000001

typedef enum				e_objs
{
	e_sphere,
	e_plane,
	e_cylinder,
	e_square,
	e_triangle,
}							t_objs;

typedef struct				s_sphere
{
	t_vector				pos;
	double					d;
	t_color					color;
}							t_sphere;

typedef struct				s_plane
{
	t_vector				pos;
	t_vector				dir;
	t_color					color;
}							t_plane;

typedef struct				s_square
{
	t_vector				pos;
	t_vector				dir;
	double					height;
	t_color					color;
}							t_square;

typedef struct				s_cyl
{
	t_vector				pos;
	t_vector				dir;
	double					d;
	double					height;
	t_color					color;
}							t_cyl;

typedef struct				s_tri
{
	t_vector				v0;
	t_vector				v1;
	t_vector				v2;
	t_color					color;
}							t_tri;

typedef struct				s_obj
{
	int						id;
	void					*objs;
	void					*start;
	t_xy					(*collision)(t_ray ray, void *obj);
	t_vector				(*get_normal)(t_vector vec, void *obj);
	struct s_obj			*next;
	t_color					color;
}							t_obj;

/*
**----------------------------SPHERE FUNCTIONS---------------------------------
*/
t_xy						rt_sphere_collision(t_ray r, void *obj);
void						rt_sphere_obj(t_obj *obj, t_sphere *s);
t_sphere					*rt_new_sphere(t_vector pos, double d,
							t_color color);

/*
**-----------------------------PLANE FUNCTIONS---------------------------------
*/
t_xy						rt_plane_collision(t_ray r, void *obj);
void						rt_plane_obj(t_obj *obj, t_plane *p);
t_plane						*rt_new_plane(t_vector pos, t_vector dir,
							t_color color);

/*
**-----------------------------SQUARE FUNCTIONS--------------------------------
*/
t_xy						rt_square_collision(t_ray r, void *obj);
void						rt_square_obj(t_obj *obj, t_square *sq);
t_square					*rt_new_square(t_vector pos,t_vector dir,
							double height, t_color color);
/*
**---------------------------CYLINDER FUNCTIONS--------------------------------
*/
t_xy						rt_cyl_collision(t_ray r, void *obj);
void						rt_cyl_obj(t_obj *obj, t_cyl *cyl);
t_cyl						*rt_new_cyl(t_ray posdir, double height, double d,
							t_color color);

/*
**---------------------------TRIANGLE FUNCTIONS--------------------------------
*/
t_xy						rt_triangle_collision(t_ray r, void *obj);
void						rt_triangle_obj(t_obj *obj, t_tri *t);
t_tri						*rt_new_triangle(t_vector v0, t_vector v1,
							t_vector v2, t_color color);

#endif
