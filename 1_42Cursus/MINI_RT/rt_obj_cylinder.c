/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   rt_obj_cylinder.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/12/08 19:54:56 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/10 04:58:43 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"
#include <stdlib.h>

t_xy			rt_cyl_collision(t_ray r, void *obj)
{
	t_cyl		*cy;
	t_xy		res;
	t_vector	abc;
	double		dot_res;
	t_vector	len;

	res.x = -1;
	res.y = -1;
	cy = (t_cyl *)obj;
	dot_res = dot(r.dir, cy->dir);
	abc.x = dot(r.dir, r.dir) - (dot_res * dot_res);
	len = vec_sub(r.pos, cy->pos);
	abc.y = 2.0 * (dot(r.dir, len) - (dot_res * dot(len, cy->dir)));
	dot_res = dot(len, cy->dir);
	abc.z = dot(len, len) - (dot_res * dot_res) - ((cy->d / 2) * (cy->d / 2));
	v_quad_equ(abc.x, abc.y, abc.z, &res);
	return (res);
}

t_vector		rt_cyl_normal(t_vector vec, void *obj)
{
	t_cyl		*cy;
	t_vector	res;
	t_vector	v;
	double		len;

	cy = (t_cyl *)obj;
	res = vec_sub(vec, cy->pos);
	len = dot(res, cy->dir);
	v = cy->dir;
	if (len < 0)
	{
		v = vec_scale(v, -1);
		len = -len;
	}
	v = vec_scale(v, len);
	res = vec_sub(res, v);
	return (normalise(res));
}

t_cyl			*rt_new_cyl(t_ray posdir, double height, double d,
		t_color color)
{
	t_cyl	*cyl;

	if (!(cyl = ft_calloc(sizeof(t_cyl), 1)))
		return (NULL);
	cyl->pos = posdir.pos;
	cyl->dir = posdir.dir;
	cyl->height = height;
	cyl->d = d;
	cyl->color = color;
	return (cyl);
}

void			rt_cyl_obj(t_obj *obj, t_cyl *cy)
{
	obj->id = e_cylinder;
	obj->collision = rt_cyl_collision;
	obj->get_normal = rt_cyl_normal;
	obj->objs = cy;
	obj->color = cy->color;
}
