/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_screen_trace.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 22:47:28 by plamtenz          #+#    #+#             */
/*   Updated: 2020/01/14 03:23:50 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "miniRT.h"
#include <stdlib.h>

static double	rt_near_t(t_xy *t)
{
	double		res;

	res = -1;
	if (t->x < 0 && t->y < 0)
		return (res);
	if ((res = t->x) < 0)
	{
		res = t->y;
		t->y = t->x;
		t->x = res;
	}
	else if (t->y >= 0 && t->y < res)
	{
		res = t->y;
		t->y = t->x;
		t->x = res;
	}
	return (res);
}

int					rt_ray_trace(t_ray r, t_scene *s, t_rt *rt, double lim_t)
{
	int			total_objs;
	double		near_t;
	t_xy		t;
	t_obj		*near_o;

	near_t = lim_t;
	near_o = NULL;
	total_objs = s->objs_num;
	while (total_objs--)
	{
		t = s->obj->collision(r, s->obj->objs);
		if (rt_near_t(&t) < 0)
			s->obj = s->obj->next;
		else if (t.x < near_t)
		{
			rt->t = t;
			near_t = t.x;
			near_o = s->obj;
			s->obj = s->obj->next;
		}
	}
	//rt->color.val = 0;
	return ((rt->obj = near_o) != NULL ? TRUE : FALSE);
}

t_vector			cam_to_point(t_ray ray, t_pixel p, t_image *i, t_cam *cam)
{
	t_vector vec;

	vec.x = (double)(p.x / 500.0) * 2.0 - 1.0;
	vec.y = (double)(p.y / 500.0) * 2.0 - 1.0;
	vec.z = 1;
	vec = vec_rotation(vec, get_rad(cam->ang));
	return (vec);
}

void				rt_engine(t_mlx *mlx, t_cam *c, t_scene *s)
{
	t_pixel		pixel;
	t_rt		rt;
	t_ray		ray;
	int i = 0;

	ray.pos = c->pos;
	int y = mlx->max_h;

	pixel.y = -1;
	int x = mlx->max_w;
	
	while (++pixel.y < y)
	{
		pixel.x = -1;
		while (++pixel.x < x)
		{

rt.color.val = 0x0000488f; // only printf that not calc obj color || init at amb light value


			//ray.dir = rt_cam_to_vp(pixel, &mlx->image, c);
			//ray.pos = vector(pixel.x, pixel.y, 0);
			//ray.dir = cam_to_point(ray, pixel, &s->win->image, c);
			// ray = set_ray(vector(pixel.x, pixel.y, 0), vector(0,0,1));
			//if (pixel.y == 500 && pixel.x == 500)
			//{
			//	printf("Ray %f\t %f\t %f\n", ray.dir.x, ray.dir.y, ray.dir.z);
			//	printf("Cam %f\t %f\t %f\n", c->pos.x, c->pos.y, c->pos.z);
			//	printf("Object %d\t %d\t %d\n\n", 500, 500, 200);
			//}


			//ray.pos.x = (2 * (pixel.x + 0.5) / (double)s->win->image.w - 1) * ((double)s->win->image.w / (double)s->win->image.h) * c->fov + c->pos.x; // must work
			//ray.pos.y = (1 - 2 * (pixel.y + 0.5) / (double)s->win->image.h) * c->fov + c->pos.y;

			ray.pos.x = pixel.x + c->pos.x - 500;
			ray.pos.y = pixel.y + c->pos.y - 500;
			ray.pos.z = 350;
			ray.dir = vector(0,0,-1);
			if (rt_ray_trace(ray, s, &rt, INFINITY))
				rt.color.val = rt_light_calc(ray, s, rt);
			pixel.color.val = rt.color.val;
			rt_pixel_to_image(&mlx->image, &pixel);
		}
	}
}
