/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_new.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 04:16:58 by plamtenz          #+#    #+#             */
/*   Updated: 2019/12/13 19:16:22 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "miniRT.h"
#include <stdlib.h>

t_obj				*rt_new_obj(int id)
{
	t_obj			*obj;

	if (!(obj = malloc(sizeof(t_obj))))
		return (NULL);
	obj->next = NULL;
	obj->start = NULL;
	obj->id = id;
	return (obj);
}

t_light				*rt_new_light(int type)
{
	t_light			*light;

	if (!(light = malloc(sizeof(t_light))))
		return (NULL);
	light->next = NULL;
	light->start = NULL;
	light->type = type;
	return (light);
}

t_scene				*rt_new_scene(void)
{
	t_scene			*scene;

	if (!(scene = malloc(sizeof(t_scene))))
		return (NULL);
	if (!(scene->win = malloc(sizeof(t_mlx))))
		return (NULL);
	scene->f = 0;
	scene->objs_num = 0;
	scene->obj = NULL;
	scene->light_num = 0;
	scene->light = NULL;
	scene->cam = NULL;
	return (scene);
}

t_cam				*rt_new_cam(int num)
{
	t_cam			*cam;

	if (!(cam = malloc(sizeof(t_cam))))
		return (NULL);
	cam->next = NULL;
	cam->back = NULL;
	cam->start = NULL;
	cam->num = num;
	cam->view_p_h = 1;
	cam->view_p_w = 1;
	cam->ang = vector(0,0,0);
	cam->rad = vector(0,0,0);
	cam->min_t = 0;
	cam->max_t = INFINITY;
	return (cam);
}

/*
void				rt_set_amb_light(t_light *light, double intesity,
		t_color color)
{
	light->type = AMBIENTAL;
	light->pos = vec(0,0,0);
	light->color.val = color_from_rgb(color.r, color.g, color.g);
	light->intesity = intesity;
	light->fadding = 0.0;
}

void				rt_set_point_light(t_light *light, t_vector v,
		double intesity, double fadding, t_color color)
{
	light->type = POINT;
	light->pos = v.pos;
	light->color.val = color_from_rgb(color.r, color.g, color.g);
	light->intesity = intesity;
	light->fadding = fadding;
}*/
