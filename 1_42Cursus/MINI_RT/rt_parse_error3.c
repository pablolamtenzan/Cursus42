/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   rt_parse_error_3.c                               .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: plamtenz <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/12/07 09:39:18 by plamtenz     #+#   ##    ##    #+#       */
/*   Updated: 2019/12/08 20:25:10 by plamtenz    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "miniRT.h"

int				rt_parse_error_square(t_obj *obj, char *line)
{
	t_square		s;
	char			**values;

	values = split_multicharset(line, " ,");
	if (!values[10] || values[11])
		return (FALSE);
	s.pos = vector(ft_atod(values[1]), ft_atod(values[2]), ft_atod(values[3]));
	if ((s.dir.x = ft_atod(values[4])) < -1 || s.dir.x > 1)
		return (FALSE);
	if ((s.dir.y = ft_atod(values[5])) < -1 || s.dir.y > 1)
		return (FALSE);
	if ((s.dir.z = ft_atod(values[6])) < -1 || s.dir.z > 1)
		return (FALSE);
	if ((s.height = ft_atod(values[7])) < 0)
		return (FALSE);
	if ((s.color.chanel.r = ft_atoi(values[8])) < 0 || s.color.chanel.r > 0xFF)
		return (FALSE);
	if ((s.color.chanel.g = ft_atoi(values[9])) < 0 || s.color.chanel.g > 0xFF)
		return (FALSE);
	if ((s.color.chanel.b = ft_atoi(values[10])) < 0 || s.color.chanel.b > 0xFF)
		return (FALSE);
	rt_square_obj(obj, rt_new_square(s.pos, s.dir,  s.height, s.color));
	return (TRUE);
}

int				rt_parse_error_triangle(t_obj *obj, char *line)
{
	t_tri			t;
	char			**values;

	values = split_multicharset(line, " ,");
	if (!values[12] || values[13])
		return (FALSE);
	t.v0 = vector(ft_atod(values[1]), ft_atod(values[2]), ft_atod(values[3]));
	t.v1 = vector(ft_atod(values[4]), ft_atod(values[5]), ft_atod(values[6]));
	t.v2 = vector(ft_atod(values[7]), ft_atod(values[8]), ft_atod(values[9]));
	if ((t.color.chanel.r = ft_atoi(values[10])) < 0 || t.color.chanel.r > 0xFF)
		return (FALSE);
	if ((t.color.chanel.g = ft_atoi(values[11])) < 0 || t.color.chanel.g > 0xFF)
		return (FALSE);
	if ((t.color.chanel.b = ft_atoi(values[13])) < 0 || t.color.chanel.b > 0xFF)
		return (FALSE);
	rt_triangle_obj(obj, rt_new_triangle(t.v0, t.v1, t.v2, t.color));
	return (TRUE);
}
