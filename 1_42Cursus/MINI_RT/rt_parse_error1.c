/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_parse_error1.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/07 03:02:03 by plamtenz          #+#    #+#             */
/*   Updated: 2019/12/16 19:14:30 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "miniRT.h"

int			rt_parse_error_res(t_scene *scene, char *line)
{
	char	**values;

	if (scene->f & RT_RES)
		return (FALSE);
	scene->f |= RT_RES;
	values = split_multicharset(line, " ,");
	if (!values[2] || values[3])
		return (FALSE);
	if ((scene->win->max_h = ft_atoi(values[1])) <= 0)
		return (FALSE);
	scene->win->max_h = scene->win->max_h > 2880 ? 2880 : scene->win->max_h;
	if ((scene->win->max_w = ft_atoi(values[2])) <= 0)
		return (FALSE);
	scene->win->max_w = scene->win->max_w > 5120 ? 5120 : scene->win->max_w;
	return (TRUE);
}

int			rt_parse_error_amb(t_light *light, char *line)
{
	char	**values;

	values = split_multicharset(line, " ,");
	if (!values[4] || values[5])
		return (FALSE);
	light->pos = vector(0, 0, 0);
	if ((light->intensity = ft_atod(values[1])) < 0 ||
			light->intensity > 1)
		return (FALSE);
	if ((light->color.chanel.r = ft_atoi(values[2])) < 0 ||
			light->color.chanel.r > 0xFF)
		return (FALSE);
	if ((light->color.chanel.g = ft_atoi(values[3])) < 0 ||
			light->color.chanel.g > 0xFF)
		return (FALSE);
	if ((light->color.chanel.b = ft_atoi(values[4])) < 0 ||
			light->color.chanel.b > 0xFF)
		return (FALSE);
	light->fadding = 0.0;
	return (TRUE);
}

int			rt_parse_error_point(t_light *light, char *line)
{
	char	**values;

	values = split_multicharset(line, " ,");
	if (!values[7] || values[8])
		return (FALSE);
	light->pos = vector(ft_atod(values[1]), ft_atod(values[2]),
			ft_atod(values[3]));
	if ((light->intensity = ft_atod(values[4])) < 0 ||
			light->intensity > 1)
		return (FALSE);
	if ((light->color.chanel.r = ft_atoi(values[5])) < 0 ||
			light->color.chanel.r > 0xFF)
		return (FALSE);
	if ((light->color.chanel.g = ft_atoi(values[6])) < 0 ||
			light->color.chanel.g > 0xFF)
		return (FALSE);
	if ((light->color.chanel.b = ft_atoi(values[7])) < 0 ||
			light->color.chanel.b > 0xFF)
		return (FALSE);
	light->fadding = 0.0; /*wtf is fadding?*/
	return (TRUE);
}

int			rt_parse_error_cam(t_cam *cam, char *line) /*where i init the other elems of cam ? */
{
	char	**values;

	values = split_multicharset(line, " ,");
	if (!values[7] || values[8])
		return (FALSE);
	cam->pos = vector(ft_atod(values[1]), ft_atod(values[2]),
			ft_atod(values[3]));
	if ((cam->dir.x = ft_atod(values[4])) < -1.0 || cam->dir.x > 1.0)
		return (FALSE);
	if ((cam->dir.y = ft_atod(values[5])) < -1.0 || cam->dir.y > 1.0)
		return (FALSE);
	if ((cam->dir.z = ft_atod(values[6])) < -1.0 || cam->dir.z > 1.0)
		return (FALSE);
	cam->dir = normalise(cam->dir);
	if ((cam->fov = ft_atod(values[7])) <=  0)
		return (FALSE);
	cam->fov = tan((cam->fov / 2) * M_PI / 180);
	return (TRUE);
}
