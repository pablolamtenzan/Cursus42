/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_convert_to_bmp.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plamtenz <plamtenz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/06 06:19:12 by plamtenz          #+#    #+#             */
/*   Updated: 2019/12/17 19:36:32 by plamtenz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "miniRT.h"

void	ft_bzero(void *str, t_size n)
{
	unsigned char *start;

	start = str;
	while (n-- > 0)
		*start++ = 0;
}

static void     start_bmp(t_image *i, t_bmp *bmp)
{
    ft_bzero(bmp, 54);
    bmp->bf_word = 0x424D;
    bmp->bf_size = 
    bmp->bi_size = 
    bmp->bi_width =
    bmp->bi_height = 
    bmp->bi_imgsize = 
    bmp->bi_bitcount = 
}

void            display_bmp(t_scene *s, int fd)
{
    t_bmp bmp;
    start_bmp(s->win->image, &bmp);
    fd = open("plamtenz'sRT.bmp", O_RDONLY | O_CREAT);
    write(fd, &bmp, 54);
    write(fd, i->str, );
}
